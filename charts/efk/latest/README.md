# Elasticsearch Chart
이 차트는 [elasticsearch/elasticsearch](https://www.docker.elastic.co/) 이미지를 기반으로 한다.
 
# Fluent-Bit Chart
[Fluent Bit](http://fluentbit.io/)는 오픈 소스 및 멀티 플랫폼 로그 전달자.

이 차트는 다음을 수행할 것이다.
* Fluent Bit에 대한 configmap 설치
* Fluent Bit를 프로비저닝하는 daemonset 설치 [호스트 아키텍처당]

# kibana
[kibana](https://github.com/elastic/kibana)는 너의 Elastic Stack의 window이다. 구체적으로, 오픈 소스(Apache Licensed), 브라우저 기반 분석 및 ElasticSearch용 검색 대시보드 입니다.

# Configurations

## Elasticsearch
다음 표에는 elasticsearch 차트의 구성 가능한 파라미터와 해당 기본값이 나열되어 있다.

|              파라미터               |                             내용                             |               기본값                |
| ------------------------------------ | ------------------------------------------------------------------- | ------------------------------------ |
| `image.repository`                   | 컨테이너 이미지 이름                                                | `docker.elastic.co/elasticsearch/elasticsearch-oss` |
| `image.tag`                          | 컨테이너 이미지 태그                                                 | `6.2.4`                              |
| `image.pullPolicy`                   | 컨테이너 pull 정책                                               | `IfNotPresent`                       |
| `master.exposeHttp`                  | 모니터링 등을 위해 마스터 파드에 http 9200 포트 노출            | `true`                               |
| `master.replicas`                    | 마스터 노드 레플리카 (스테이트풀셋)                                  | `3`                                  |
| `master.resources`                   | 마스터 노드 리소스 요청 & 제한                             | `{} - cpu limit must be an integer`  |
| `master.heapSize`                    | 마스터 노드 힙 사이즈                                               | `512m`                               |
| `master.name`                        | 마스터 컴포넌트 이름                                               | `master`                             |
| `master.persistence.enabled`         | 마스터 영구 볼륨 활성/비활성                                  | `true`                               |
| `master.persistence.name`            | 마스터 스테이트풀셋 PVC 템플릿 이름                                | `data`                               |
| `master.persistence.size`            | 마스터 영구 볼륨 크기                                       | `10Gi`                                |
| `master.persistence.storageClass`    | 마스터 영구 볼륨 클래스                                      | `nil`                                |
| `master.persistence.accessMode`      | 마스터 영구 볼륨 액세스 모드                                       | `ReadWriteOnce`                      |
| `master.antiAffinity`                | 데이터 정책                                           | `soft`                               |
| `rbac.create`                        | 쿠버네티스 플러그인에 대한 서비스 계정 및 클러스터 역할 바인딩 생성 | `false`                              |


## Kibana
다음 표에는 키바나 차트의 구성 가능한 파라미터와 기본값이 나열되어 있다.

파라미터 | 내용 | 기본값
--- | --- | ---
`affinity` | 노드/파드 선호 | None
`env` | 키바나 구성을 위한 환경 변수 | `{}`
`image.pullPolicy` | 이미지 풀 정책 | `IfNotPresent`
`image.repository` | 이미지 레포지토리 | `kibana`
`image.tag` | 이미지 태그 | `6.0.0`
`image.pullSecrets` | 이미지 풀 시크릿 지정 | `nil`
`commandline.args` | 추가적인 커맨드 인수 추가 | `nil`
`ingress.enabled` | 인그레스 활성 | `false`
`ingress.annotations` | 인그레스 어노테이션 | None:
`ingress.hosts` | 인그레스 호스트 네임 | None:
`ingress.tls` | 인그레스 TLS 구성 | None:
`nodeSelector` | 파드 할당을 위한 노드 라벨 | `{}`
`podAnnotations` | 각 파드에 추가할 어노테이션 | `{}`
`replicaCount` | 원하는 파드 수 | `1`
`resources` | 파드 리소스 요청 & 제한 | `{}`
`service.externalPort` | 서비스 외부 포트 | `443`
`service.internalPort` | 서비스 내부 포트 | `4180`
`service.externalIPs` | 외부 IP 주소 | None:
`service.loadBalancerIP` | 로드 밸런서 IP 주소 (service.type LoadBalancer일 때 사용) | None:
`service.type` | 서비스 타입 | `ClusterIP`
`service.annotations` | 쿠버네티스 서비스 어노테이션 | None:
`tolerations` | 허용할 노드 리스트 | `[]`


## Fluent-Bit
다음 표에는 Fluent-Bit 차트의 구성 가능한 파라미터와 기본값이 나열되어 있다.

| 파라미터                  | 내용                        | 기본값                 |
| -----------------------    | ---------------------------------- | ----------------------- |
| **백엔드 셀렉션**      |
| `backend.type`             | Fluent-Bit가 수집한 정보를 flush할 백엔드 설정 | `forward` |
| **포워드 백엔드**        |
| `backend.forward.host`     | Fluent-Bit 또는 Fluentd가 포워드 메시지를 수신하는 타겟 호스트 | `fluentd` |
| `backend.forward.port`     | 타겟 서비스의 TCP 포트 | `24284` |
| **ElasticSearch 백엔드**  |
| `backend.es.host`          | 타겟 Elasticsearch 인스턴스의 IP 주소 또는 호스트 네임 | `elasticsearch` |
| `backend.es.port`          | 타겟 Elasticsearch 인스턴스의 TCP 포트 | `9200` |
| `backend.es.index`         | Elastic 인덱스 이름 | `kubernetes_cluster` |
| `backend.es.type`          | Elastic 타입 네임 | `flb_type` |
| `backend.es.logstash_prefix`  | 인덱스 Prefix. Logstash_Prefix가  'mydata'라고 하면, 인덱스는 'mydata-YYYY.MM.DD'가 됨  | `kubernetes_cluster` |
| `backend.es.http_user`        | Elastic X-Pack 액세스에 대한 선택적 유저 네임 자격증명 | `` |
| `backend.es.http_passwd:`     | HTTP_User에 정의된 사용자의 비밀번호 | `` |
| `backend.es.tls`              | TLS 지원 활성화 또는 비활성화 | `off` |
| `backend.es.tls_verify`       | 인증서 유효성 검사  | `on` |
| `backend.es.tls_ca`           | Elastic 인스턴스에 대한 TLS CA 인증서(PEM 형식). tls: on인지 지정. | `` |
| `backend.es.tls_debug`        | TLS 디버그 상세 수준 설정. 다음 값을 수락함: 0-4 | `1` |
| **HTTP 백엔드**              |
| `backend.http.host`           | 타겟 HTTP 서버의 IP 주소 또는 호스트 네임 | `127.0.0.1` |
| `backend.http.port`           | 타겟 HTTP 서버의 TCP 포트 | `80` |
| `backend.http.uri`            | 타겟 웹 서버에 대한 선택적 HTTP URI를 지정, e.g: /something | `"/"`
| `backend.http.format`         | HTTP request body에서 사용되는 데이터 형식을 지정, 기본값은 msgpack을 사용하고, 선택적으로 json을 설정 가능 | `msgpack` |
| **파서**                   |
| `parsers.regex`                    | 정규식 파서 목록 | `NULL` |
| `parsers.json`                     | json 파서 목록 | `NULL` |
| **일반**                   |
| `annotations`                      | 선택적 데몬셋 세트 어노테이션        | `NULL`                |
| `podAnnotations`                   | 선택적 파드 어노테이션                  | `NULL`                |
| `existingConfigMap`                | 컨피그맵 재정의                         | ``                    |
| `extraVolumeMounts`                | elasticsearch에서 tls가 활성화된 경우, ssl 인증서 마운트에 필요한 별도의 볼륨 마운트 |          |
| `extraVolume`                      | 별도의 볼륨                               |                                                |
| `filter.kubeURL`                   | 선택적 커스텀 컨피그맵                 | `https://kubernetes.default.svc:443`            |
| `filter.kubeCAFile`                | 선택적 커스텀 컨피그맵       | `/var/run/secrets/kubernetes.io/serviceaccount/ca.crt`    |
| `filter.kubeTokenFile`             | 선택적 커스텀 컨피그맵       | `/var/run/secrets/kubernetes.io/serviceaccount/token`     |
| `filter.kubeTag`                   | 필터에서 매칭을 위한 선택적 최상위 태그         | `kube`                                 |
| `image.fluent_bit.repository`      | 이미지                                      | `fluent/fluent-bit`                               |
| `image.fluent_bit.tag`             | 이미지 태그                                  | `0.13.0`                                          |
| `image.pullPolicy`                 | 이미지 pull 정책                          | `Always`                                          |
| `rbac.create`                      | RBAC 리소스가 생성되어야 하는지 지정   | `true`                                 |
| `serviceAccount.create`            | ServiceAccount가 생성되어야 하는지 지정 | `true`                                 |
| `serviceAccount.name`              | 사용할 ServiceAccount 이름     | `NULL`                                            |
| `resources.limits.cpu`             | CPU 제한                                  | `100m`                                            |
| `resources.limits.memory`          | Memory 제한                               | `500Mi`                                           |
| `resources.requests.cpu`           | CPU 요청                                | `100m`                                            |
| `resources.requests.memory`        | Memory 요청                             | `200Mi`                                           |
| `tolerations`                      | 선택적 데몬셋 허용             | `NULL`                                            |
| `nodeSelector`                     | fluent-bit 파드 할당을 위한 노드 라벨  | `NULL`                                            |
| `metrics.enabled`                  | 메트릭에 대한 서비스가 노출되어야 하는지 지정 | `false`                            |
| `metrics.service.port`             | 메트릭이 노출해야 하는 포트    | `2020`                                            |
| `metrics.service.type`             | 메트릭의 서비스 타입                   | `ClusterIP`                                       |
| | | |
