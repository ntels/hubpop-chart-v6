# Hadoop 차트

[Hadoop](https://hadoop.apache.org/)은 대규모 분산 애플리케이션을 실행하기 위한 프레임워크다.

이 차트는 주로 YARN 및 MapReduce 작업 실행에 사용되며, 여기서 HDFS는 분산 파일 시스템이 아닌 프레임워크 내에서 작은 아티팩트를 전송하기 위한 수단으로 사용된다. 데이터는 Google 클라우드 스토리지, S3 또는 Swift와 같은 클라우드 기반 데이터스토어에서 읽어야 한다.

## Chart 상세

## Chart 설치

사용 가능한 노드 리소스의 50%를 사용하는 릴리스 이름 `hadoop`로 차트를 설치하려면 다음과 같이 하십시오:

```
$ helm install --name hadoop $(stable/hadoop/tools/calc_resources.sh 50) stable/hadoop
```

> 노드 관리자 포드당 최소 2GB의 사용 가능한 메모리가 필요하며, 클러스터가 충분히 크지 않으면 모든 포드가 예약되지 않는다는 점에 유의하십시오.

선택적인 [`calc_resources.sh`](./tools/calc_resources.sh) 스크립트는 `yarn.numNodes`를 설정하는 편의 도우미로 사용되며, `yarn.nodeManager.resources`은 쿠버네티스 클러스터의 모든 노드와 일정 비율의 자원을 적절히 활용한다. 예를 들어, 3 노드 `n1-standard-4` GKE 클러스터와 `50` 인수를 사용하면 코어 2개와 메모리 7.5Gi를 요구하는 3개의 NodeManager 포드가 생성된다.

## Configuration

다음 표에는 Hadoop 차트의 구성 가능한 파라미터와 기본값이 나열되어 있다.

| 파라미터                                         | 내용                                                                        | 기본값                                                          |
| ------------------------------------------------- | -------------------------------                                                    | ---------------------------------------------------------------- |
| `image.repository`                                           | 하둡 이미지 ([소스](https://github.com/Comcast/kube-yarn/tree/master/image))    | `danisla/hadoop`                                       |
| `image.tag`                                   | 사용될 하둡 라이브러리 버전                                              | `{VERSION}`                                                      |
| `image.pullPolicy`                                 | 이미지에 대한 풀 정책                                                         | `IfNotPresent`                                                   |
| `antiAffinity`                                    | 파드 antiaffinity, `hard` 또는 `soft`                                                 | `hard`                                                           |
| `hdfs.nameNode.pdbMinAvailable`                   | HDFS 네임 노드에 대한 PDB                                                              | `1`                                                              |
| `hdfs.nameNode.resources`                         | HDFS 네임 노드에 대한 리소스                                                    | `requests:memory=256Mi,cpu=10m,limits:memory=2048Mi,cpu=1000m`   |
| `hdfs.dataNode.replicas`                          | HDFS 데이터 노드 레플리카 수                                                   | `1`                                                              |
| `hdfs.dataNode.pdbMinAvailable`                   | HDFS 데이터 노드에 대한 PDB                                                              | `1`                                                              |
| `hdfs.dataNode.resources`                         | HDFS 데이터 노드에 대한 리소스                                                    | `requests:memory=256Mi,cpu=10m,limits:memory=2048Mi,cpu=1000m`   |
| `yarn.resourceManager.pdbMinAvailable`            | YARN 리소스 매니저에 대한 PDB                                                   | `1`                                                              |
| `yarn.resourceManager.resources`                  | YARN 리소스 매니저에 대한 리소스                                             | `requests:memory=256Mi,cpu=10m,limits:memory=2048Mi,cpu=1000m`   |
| `yarn.nodeManager.pdbMinAvailable`                | YARN 노드 매니저에 대한 PDB                                                       | `1`                                                              |
| `yarn.nodeManager.replicas`                       | YARN 노드 매니저 레플리카 수                                                | `2`                                                              |
| `yarn.nodeManager.parallelCreate`                 | 모든 노드매니저 스테이트풀셋 파드를 병렬로 생성 (K8S 1.7+)                     | `false`                                                          |
| `yarn.nodeManager.resources`                      | YARN 노드 매니저 파드에 대한 리소스 제한 및 요청                             | `requests:memory=2048Mi,cpu=1000m,limits:memory=2048Mi,cpu=1000m`|
| `persistence.nameNode.enabled`                    | 영구 볼륨 활성/비활성                                                   | `false`                                                          | 
| `persistence.nameNode.storageClass`               | 볼륨 프로바이더별로 사용할 스토리지 클래스 이름                           | `-`                                                              |
| `persistence.nameNode.accessMode`                 | 볼륨에 대한 액세스 모드                                                         | `ReadWriteOnce`                                                  |
| `persistence.nameNode.size`                       | 볼륨의 크기                                                                 | `50Gi`                                                           |
| `persistence.dataNode.enabled`                    | 영구 볼륨 활성/비활성                                                   | `false`                                                          | 
| `persistence.dataNode.storageClass`               | 볼륨 프로바이더별로 사용할 스토리지 클래스 이름                           | `-`                                                              |
| `persistence.dataNode.accessMode`                 | 볼륨에 대한 액세스 모드                                                         | `ReadWriteOnce`                                                  |
| `persistence.dataNode.size`                       | 볼륨의 크기                                                                 | `200Gi`                                                          |

## 관련 차트

[Zeppelin Notebook](https://github.com/kubernetes/charts/tree/master/stable/zeppelin) 차트는 hadoop 클러스터에 대해 hadoop 구성을 사용할 수 있으며 YARN 실행자를 사용할 수 있음:

```
helm install --set hadoop.useConfigMap=true stable/zeppelin
```

# 참조

- 오리지날 K8S Hadoop adaptation 차트는 https://github.com/Comcast/kube-yarn에서 도출한 것이다.
