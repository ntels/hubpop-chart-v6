## Configuration

다음 표는 Memcached 차트의 구성 가능한 매개 변수 및 해당 기본값을 나열합니다.

|      파라미터            |          내용            |                         기본값                         |
|---------------------------|---------------------------------|---------------------------------------------------------|
| `image`                   | 가져와서 실행할 이미지       | 최근 공식 memcached 태그                         |
| `imagePullPolicy`         | 이미지 풀 정책               | `imageTag`가 `latest`라면 `Always`, 아니면 `IfNotPresent` |
| `memcached.verbosity`     | 상세 수준 (v, vv, 또는 vvv) | 미설정                                                 |
| `memcached.maxItemMemory` | 아이템에 대한 최대 메모리 (MB)    | `64`                                                    |
| `metrics.enabled`         | 프로메테우스 포맷에 메트릭 노출 | false                                               |
| `metrics.image`           | 가져와서 실행할 메트릭 이미지 | 최근 공식 memcached 태그      |
| `metrics.imagePullPolicy` | 이미지 풀 정책               | `imageTag`가 `latest`라면 `Always`, 아니면 `IfNotPresent` |
| `metrics.resources`       | 메트릭 exporter에 대한 CPU/Memory 리소스 요청/제한 | `{}`                       |
| `extraContainers`         | 문자열로 컨테이너 사이드카 정의 | 미설정                                        |
| `extraVolumes`            | 문자열로 추가할 볼륨 정의 | Un-set                                              |

위의 파라미터는 `memcached` 파라미터에 매핑됩니다. 자세한 내용은 [Memcached documentation](https://github.com/memcached/memcached/wiki/ConfiguringServer)를 참조하십시오.

`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오. 예를 들면,

```bash
$ helm install --name my-release \
  --set memcached.verbosity=v \
    stable/memcached
```

위 명령은 Memcached 세부 정보를`v`로 설정합니다.

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면,

```bash
$ helm install --name my-release -f values.yaml stable/memcached
```

> **Tip**: 기본값 [values.yaml](values.yaml)을 사용할 수 있습니다.
