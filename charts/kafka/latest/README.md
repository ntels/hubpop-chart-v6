# 아파치 Kafka 헬름 차트

이것은 여기에서 찾은 Kafka StatefulSet의 구현법이다.

 * https://github.com/Yolean/kubernetes-kafka

## StatefulSet Caveats

* https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/#limitations

### Installing the Chart
이 차트는 기본적으로 `requirement.yaml`의 kafka 클러스터에 대한 의존도로서 ZooKeeper 차트를 포함하고 있다. 다음 구성 가능한 매개 변수를 사용하여 차트를 사용자 정의할 수 있음:

| 파라미터                      | 내용                                                                                                     | 기본값                                                    |
| ------------------------------ | --------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------- |
| `image`                        | Kafka 컨테이너 이미지 이름                                                                                      | `confluentinc/cp-kafka`                                    |
| `imageTag`                     | Kafka 컨테이너 이미지 태그                                                                                       | `4.0.0`                                                    |
| `imagePullPolicy`              | Kafka 컨테이너 풀 정책                                                                                     | `IfNotPresent`                                             |
| `replicas`                     | Kafka 브로커                                                                                                   | `3`                                                        |
| `component`                    | Kafka 쿠버네티스 셀렉터 키                                                                                          | `kafka`                                                    |
| `resources`                    | Kafka 리소스 요청 및 제한                                                                              | `{}`                                                       |
| `kafkaHeapOptions`             | Kafka 브로커 JVM 힙 옵션                                                                                   | `-Xmx1G-Xms1G`                                                       |
| `logSubPath`                   | Kafka 로그가 위치할 `persistence.mountPath` 아래 하위 경로                                          | `logs`                                                     |
| `schedulerName`                | 쿠버네티스 스케줄러 이름 (기본값 이외)                                                           | `nil`                                                      |
| `affinity`                     | 파드에 대한 affinities 및 anti-affinities 정의 https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity preferences                                                                                      | `{}`                                                       |
| `tolerations`                  | 파드에 대한 노드 허용 리스트 https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/  | `[]`                                                       |
| `external.enabled`             | True라면, Kafka broker가 NodePort로 노출 (PLAINTEXT by default)                                              | `false`                                                    |
| `external.servicePort`         | 노드포트에서 외부 리스너 포트로 릴레이하기 위해 외부 서비스(파드당 하나)로 구성된 TCP 포트    | '19092'                                                    |
| `external.firstListenerPort`   | 노드포트 및 외부 리스너 포트에 사용되는 포트에 도착하기 위해 파드 인덱스 넘버가 추가된 TCP 포트    | '31090'                                                    |
| `external.domain`              | Kafka 외부 리스너를 알리는 도메인                                                          | `cluster.local`                                            |
| `external.init`                | 외부 init 컨테이너 설정                                                                               | (`values.yaml` 참조)                                        |
| `rbac.enabled`                 | RBAC 가능 클러스터에서 init 컨테이너가 사용할 서비스 계정 및 역할 활성화                      | `false`                                                    |
| `configurationOverrides`       | `Kafka ` [구성 설정][브로커 컨피그] dictionary 형식으로 재정의                              | `{ offsets.topic.replication.factor: 3 }`                  |
| `additionalPorts`              | 브로커에 노출할 추가 포트. 이미지가 사이드카 대신 자바 에이전트를 통해 메트릭 (예 : 프로메테우스 등)을 노출 할 때 유용   | `{}`                                 |
| `readinessProbe.initialDelaySeconds` | 프로브가 시작되기 전 시간(초)                                                              | `30`                                                       |
| `readinessProbe.periodSeconds`       | 프로브가 수행되는 빈도(초)                                                              | `10`                                                       |
| `readinessProbe.timeoutSeconds`      | 프로브가 시간 초과된 시간(초)                                                        | `5`                                                        |
| `readinessProbe.successThreshold`    | 프로브가 실패한 후 성공한 것으로 간주되는 최소 연속 성공              | `1`                                                        |
| `readinessProbe.failureThreshold`    | 프로브가 여러 번 실패하면 파드에 Unready로 표시                                        | `3`                                                        |
| `terminationGracePeriodSeconds`      | 브로커가 정상적으로 종료될 때까지 이 수 초 동안 기다렸다가 종료               | `60`                                                        |
| `updateStrategy`               | 사용할 스테이트풀셋 업데이트 전략                                                                             | `{ type: "OnDelete" }`                                     |
| `podManagementPolicy`          |               | `OrderedReady`                                     |
| `persistence.enabled`          | 영구 데이터에 사용할 PVC                                                                                       | `true`                                                     |
| `persistence.size`             | 데이터 볼륨 크기                                                                                             | `1Gi`                                                      |
| `persistence.mountPath`        | 데이터 볼륨의 마운트 경로                                                                                       | `/opt/kafka/data`                                          |
| `persistence.storageClass`     | 백업 PVC의 스토리지 클래스                                                                                    | `nil`                                                      |
| `jmx.configMap.enabled`      | JMX에 대한 기본값 컨피그맵 활성화                                                                   | `true`                                                     |
| `jmx.configMap.overrideConfig` | 컨피그맵에 값은 전달하여 컨피그 파일을 생성 가능                                     | `{}`                                                       |
| `jmx.configMap.overrideName` | 사용할 컨피그맵의 이름 설정                                                     | `""`                                                       |
| `jmx.port`                      | JMX 스타일 메트릭이 노출되는 JMX 포트 (참고: 프로메테우스에서 스크랩할 수 없음)                 | `5555`                                                     |
| `jmx.whitelistObjectNames` | JMX 스탯을 통해 JMX Exporter에 노출하려는 JMX 객체를 설정 가능                      | (`values.yaml` 참조)                                                       |
| `prometheus.jmx.resources`      | JMX 사이드카 컨테이너에 대한 리소스 제한을 설정 가능                                                        | `{}`                                                       |
| `prometheus.jmx.enabled`          | JMX 메트릭을 프로메테우스에 노출시킬지 여부                                                           | `false`                                                    |
| `prometheus.jmx.image`            | JMX Exporter 컨테이너 이미지                                                                                 | `solsson/kafka-prometheus-jmx-exporter@sha256`             |
| `prometheus.jmx.imageTag`         | JMX Exporter 컨테이너 이미지 태그                                                                             | `a23062396cd5af1acdf76512632c20ea6be76885dfc20cd9ff40fb23846557e8` |
| `prometheus.jmx.interval`         | Prometheus operator를 사용할 때 Prometheus가 JMX 메트릭을 스크랩하는 간격                                  | `10s`                                                     |
| `prometheus.jmx.port`             | 스크랩을 위해 Prometheus 형식으로 메트릭을 노출하는 JMX exporter 포트                                    | `5556`                                                     |
| `prometheus.kafka.enabled`        | 별도의 Kafka exporter를 생성할지 여부                                                           | `false`                                                    |
| `prometheus.kafka.image`          | Kafka Exporter 컨테이너 이미지                                                                               | `danielqsj/kafka-exporter`                                 |
| `prometheus.kafka.imageTag`       | Kafka Exporter 컨테이너 이미지 태그                                                                           | `v1.0.1`                                                   |
| `prometheus.kafka.interval`       | Prometheus operator를 사용할 때 Prometheus가 Kafka 메트릭을 스크랩하는 간격                                 | `10s`                                                      |
| `prometheus.kafka.port`        | 스크랩을 위해 Prometheus 형식으로 메트릭을 노출하는 Kafka exporter 포트                                     | `9308`                                                     |
| `prometheus.kafka.resources`    | kafka-exporter 파드에 대한 리소스 제한을 설정 가능                                                           | `{}`                                                       |
| `prometheus.operator` | True이면, Prometheus Operator를 사용, False이면 미사용                                                             | `false`                                                       |
| `prometheus.operator.serviceMonitor.namespace` | Prometheus가 실행 중인 네임 스페이스. 기본값은 kube-prometheus install.    | `monitoring` |
| `prometheus.operator.serviceMonitor.selector` | 기본적으로 kube-prometheus 설치 (CoreOS 권장), 하지만 Prometheus 설치에 따라 설정 필요    | `{ prometheus: kube-prometheus }` |
| `zookeeper.enabled`            | True라면, Zookeeper 차트 설치                                                                               | `true`                                                     |
| `zookeeper.resources`          | Zookeeper 리소스 요청 및 제한                                                                          | `{}`                                                       |
| `zookeeper.heap`               | Zookeeper에 할당할 JVM heap 크기                                                                          | `1G`                                                       |
| `zookeeper.storage`            | Zookeeper 영구 볼륨 크기                                                                                | `2Gi`                                                      |
| `zookeeper.imagePullPolicy`    | Zookeeper 컨테이너 pull 정책                                                                                 | `IfNotPresent`                                             |
| `zookeeper.url`                | Zookeeper 클러스터의 URL (Zookeeper 차트를 설치하는 경우 불필요)                                               | `""`                                                       |
| `zookeeper.port`               | Zookeeper 클러스터의 포트                                                                                       | `2181`                                                     |
| `zookeeper.affinity`                     | 파드에 대한 affinities 및 anti-affinities를 정의: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity 환경설정                                                                                      | `{}`

`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오.

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면:

```bash
$ helm install --name my-kafka -f values.yaml incubator/kafka
```

### Kubernetes 내부에서 Kafka로 연결

다음과 같은 구성으로 K8s 클러스터에서 다음과 같은 간단한 포드를 실행하면 카프카에 연결할 수 있다:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: testclient
  namespace: kafka
spec:
  containers:
  - name: kafka
    image: solsson/kafka:0.11.0.0
    command:
      - sh
      - -c
      - "exec tail -f /dev/null"
```

위의 테스트 클라이언트 포드를 실행하면 다음과 같은 모든 카프카 항목을 나열할 수 있다:

` kubectl -n kafka exec -ti testclient -- ./bin/kafka-topics.sh --zookeeper
my-release-zookeeper:2181 --list`

여기서 `my-release`는 당신의 헬름 릴리즈 네임이다.

## 확장

카프카는 풍부한 생태계를 가지고 있고, 많은 도구들을 가지고 있다. 본 섹션은 해당 헬름 차트가 이미 작성된 모든 도구를 컴파일하기 위한 것이다.

- [Schema-registry](https://github.com/kubernetes/charts/tree/master/incubator/schema-registry) -  메타데이터에 대한 서비스 계층을 제공하는 결합 프로젝트. 그것은 Avro 스키마를 저장하고 검색하기 위한 RESTful 인터페이스를 제공한다.

### Kubernetes 외부에서 Kafka에 연결

`values.yaml`의 외부 액세스와 관련된 예제 텍스트를 사용하려면 검토하고 선택적으로 대체하십시오.

구성되면 복제 본당 하나씩 NodePorts를 통해 Kafka에 연결할 수 있어야합니다. 개인, 토폴로지가 사용되는 kops에서 이 기능은 다음 이름 지정 체계를 사용하여 내부 라운드 로빈 DNS 레코드를 게시합니다. 이 차트의 외부 접근 기능은 플란넬 네트워킹을 사용하여 AWS의 kops로 테스트되었다.
kops로 실행되는 카프카에 대한 외부 접근을 가능하게 하려면, 당신의 보안 그룹을 비 쿠버넷 노드(예: bastion)가 카프카 외부 수신기 포트 범위에 접근할 수 있도록 조정해야 할 것이다.

```
{{ .Release.Name }}.{{ .Values.external.domain }}
```

컨테이너와 NodePort에서 사용되는 외부 접근을 위한 포트 번호는 StatefulSet의 각 컨테이너에 고유하다.
`replicas` 값이 `3`인 기본 `external.firstListenerPort` 번호를 사용하여 외부 액세스를 위해 다음 컨테이너와 NodePorts를 여십시오: `31090`, `31091`, `31092`. 쿠버네티스는 각 노드포트를 동일한 포트에서 수신하는 엔트리 노드에서 포드/컨테이너로 라우팅하기 때문에 이러한 모든 포트는 호스트에서 노드포트로 연결할 수 있어야 한다(예: '31091').

각 외부 접속 서비스(팟당 하나의 서비스)의 `external.servicePort`는 각각의 `NodePort`와 일치하는 번호를 가진 `external.servicePort` 쪽으로 중계되는 것이다. NodePort의 범위는 StatefulSet의 모든 Kafka 포드에서 설정되지만 실제로 수신해서는 안됩니다. 특정 포드는 한 번에 하나의 포트만 수신하므로 모든 Kafka 포드에서 범위를 설정하는 것이 합리적으로 안전한 구성입니다.

## 알려진 제한

* 주제 작성이 자동화되지 않습니다.
* 영구 볼륨 클레임에 대한 백엔드가있는 스토리지 옵션 만 지원 (대부분 AWS에서 테스트)
* 동일한 네임 스페이스에 `kafka`라는 서비스가 없어야합니다.

[brokerconfigs]: https://kafka.apache.org/documentation/#brokerconfigs

## Prometheus 통계

### Prometheus vs Prometheus Operator

Standard Prometheus가 이 차트의 기본 모니터링 옵션입니다. 이 차트는 CoreOS Prometheus Operator를 지원하며 Prometheus 및 Alert Manager 구성 자동 업데이트와 같은 추가 기능을 제공 할 수 있습니다. Prometheus Operator 설치에 관심이있는 경우 자세한 내용은 [CoreOS repository](https://github.com/coreos/prometheus-operator/tree/master/helm)를 참조하거나 [CoreOS blog post introducing the Prometheus Operator](https://coreos.com/blog/the-prometheus-operator.html)를 읽으십시오.

### JMX Exporter

Kafka 통계의 대부분은 JMX를 통해 제공되며 [Prometheus JMX Exporter](https://github.com/prometheus/jmx_exporter)를 통해 노출됩니다.

JMX Exporter는 모든 Java 응용 프로그램과 함께 사용하기 위한 범용 프로 메테우스 공급자입니다. 이로 인해 관심이 없을 수 있는 많은 통계가 생성됩니다. 이러한 통계를 관련 구성 요소로 줄이는 데 도움을 주기 위해 JMX 내보내기를 위한 선별 된 화이트리스트 `whitelistObjectNames`을 작성했습니다. 이 화이트리스트는 값 구성을 통해 수정하거나 제거 할 수 있습니다.

이 차트는 Prometheus 지표와의 호환성을 위해 원시 JMX 지표의 변환을 수행합니다. 예를 들어, 브로커 이름 및 주제 이름이 라벨이 아닌 측정 항목 이름으로 통합되었습니다.
차트 메트릭에 대한 기본 변환에 대해 더 궁금한 점이 있으면 [configmap template](https://github.com/kubernetes/charts/blob/master/incubator/kafka/templates/jmx-configmap.yaml)를 참조하십시오.

### Kafka Exporter

[Kafka Exporter](https://github.com/danielqsj/kafka_exporter)은 JMX Exporter에 대한 무료 메트릭스 익스포터입니다. Kafka Exporter는 Kafka Consumer Groups에 대한 추가 통계를 제공합니다.
