# 워드 프레스

[WordPress](https://wordpress.org/)는 시장에서 가장 다양한 오픈 소스 컨텐츠 관리 시스템 중 하나입니다. 블로그 및 웹 사이트를 구축하기위한 출판 플랫폼입니다.

## TL;DR;

```console
$ helm install stable/wordpress
```

## 소개

이 차트는 [Helm](https://helm.sh) 패키지 매니저를 사용하여 [Kubernetes](http://kubernetes.io) 클러스터에 [WordPress](https://github.com/bitnami/bitnami-docker-wordpress) 배포를 부트 스트랩합니다.

또한 WordPress 응용 프로그램의 데이터베이스 요구 사항을 위해 MariaDB 배포를 부트 스트랩하는 데 필요한 [Bitnami MariaDB chart](https://github.com/kubernetes/charts/tree/master/stable/mariadb)도 패키지화합니다.

## 전제 조건

- 베타 API가 활성화 된 Kubernetes 1.4 이상
- 기본 인프라에서 PV 프로비저너 지원

## Chart 설치

차트 릴리즈 이름을 `my-release`로 설치할 때:

```console
$ helm install --name my-release stable/wordpress
```

이 명령은 기본 구성으로 Kubernetes 클러스터에 WordPress를 배포합니다. [configuration](#configuration) 섹션에는 설치 중에 구성 할 수있는 매개 변수가 나열됩니다.

> **Tip**: `helm list`를 사용하여 모든 릴리즈 목록 조회

## Chart 삭제

`my-release` 배포를 제거 / 삭제하려면:

```console
$ helm delete my-release
```

이 명령은 차트와 관련된 모든 Kubernetes 구성 요소를 제거하고 릴리스를 삭제합니다.

## 구성

다음 표는 WordPress 차트의 구성 가능한 매개 변수 및 해당 기본값을 나열합니다.

| 파라미터                            | 내용                                | 기본값                                                    |
| ------------------------------------ | ------------------------------------------ | ---------------------------------------------------------- |
| `image.registry`                     | WordPress 이미지 레지스트리                   | `docker.io`                                                |
| `image.repository`                   | WordPress 이미지 이름                       | `bitnami/wordpress`                                        |
| `image.tag`                          | WordPress 이미지 태그                        | `{VERSION}`                                                |
| `image.pullPolicy`                   | 이미지 풀 정책                          | `imageTag`가 `latest`라면 `Always`, 아니면 `IfNotPresent`    |
| `image.pullSecrets`                  | 이미지 풀 시크릿 지정                 | `nil`                                                      |
| `wordpressUsername`                  | 어플리케이션 유저                    | `user`                                                     |
| `wordpressPassword`                  | 어플리케이션 패스워드                       | _임의의 영숫자 10글자 문자열_             |
| `wordpressEmail`                     | 관리자 이메일                                | `user@example.com`                                         |
| `wordpressFirstName`                 | 퍼스트 네임                                 | `FirstName`                                                |
| `wordpressLastName`                  | 라스트 네임                                  | `LastName`                                                 |
| `wordpressBlogName`                  | 블로그 네임                                  | `User's Blog!`                                             |
| `wordpressTablePrefix`               | 테이블 prefix                               | `wp_`                                                      |
| `allowEmptyPassword`                 | DB 빈 암호 허용                   | `yes`                                                      |
| `smtpHost`                           | SMTP 호스트                                  | `nil`                                                      |
| `smtpPort`                           | SMTP 호스트                                  | `nil`                                                      |
| `smtpUser`                           | SMTP 유저                                  | `nil`                                                      |
| `smtpPassword`                       | SMTP 패스워드                              | `nil`                                                      |
| `smtpUsername`                       | SMTP 이메일에 대한 유저 네임                  | `nil`                                                      |
| `smtpProtocol`                       | SMTP 프로토콜 [`tls`, `ssl`]               | `nil`                                                      |
| `mariadb.enabled`                    | MariaDB 컨테이너 배포                | `true`                                                     |
| `mariadb.mariadbRootPassword`        | MariaDB 관리자 패스워드                     | `nil`                                                      |
| `mariadb.mariadbDatabase`            | 생성할 데이터베이스 이름                    | `bitnami_wordpress`                                        |
| `mariadb.mariadbUser`                | 생성할 데이터베이스 유저                    | `bn_wordpress`                                             |
| `mariadb.mariadbPassword`            | 데이터베이스 패스워드                  | _임의의 영숫자 10글자 문자열_             |
| `externalDatabase.host`              | 외부 데이터베이스 호스트              | `localhost`                                                |
| `externalDatabase.user`              | 외부 DB의 기존 유저       | `bn_wordpress`                                             |
| `externalDatabase.password`          | 위 유저에 대한 패스워드            | `nil`                                                      |
| `externalDatabase.database`          | 기존 데이터베이스 이름              | `bitnami_wordpress`                                        |
| `externalDatabase.port`              | 데이터베이스 포트 넘버                       | `3306`                                                     |
| `serviceType`                        | 쿠버네티스 서비스 타입                    | `LoadBalancer`                                             |
| `nodePorts.http`                     | 쿠버네티스 http 노드 포트                  | `""`                                                       |
| `nodePorts.https`                    | 쿠버네티스 https 노드 포트                 | `""`                                                       |
| `healthcheckHttps`                   | liveliness 및 readiness를 위해 https 사용     | `false`                                                    |
| `ingress.enabled`                    | 인그레스 컨트롤러 리소스 활성         | `false`                                                    |
| `ingress.hosts[0].name`              | WordPress 설치의 호스트 이름    | `wordpress.local`                                          |
| `ingress.hosts[0].path`              | URL 구조 내 경로              | `/`                                                        |
| `ingress.hosts[0].tls`               | 인그레스에서 TLS 백엔드 활용             | `false`                                                    |
| `ingress.hosts[0].tlsSecret`         | TLS 시크릿 (인증서)                  | `wordpress.local-tls-secret`                               |
| `ingress.hosts[0].annotations`       | 이 호스트의 인그레스 레코드에 대한 어노테이션 | `[]`                                                       |
| `ingress.secrets[0].name`            | TLS 시크릿 이름                            | `nil`                                                      |
| `ingress.secrets[0].certificate`     | TLS 시크릿 인증서                     | `nil`                                                      |
| `ingress.secrets[0].key`             | TLS 시크릿 키                             | `nil`                                                      |
| `persistence.enabled`                | PVC를 사용하여 영구 볼륨 활성               | `true`                                                     |
| `persistence.existingClaim`          | 기존 PVC를 사용하여 영구 볼륨 활성   | `nil`                                                      |
| `persistence.storageClass`           | PVC 스토리지 클래스                          | `nil` (알파 스토리지 클래스 어노테이션 사용)                |
| `persistence.accessMode`             | PVC 액세스 모드                            | `ReadWriteOnce`                                            |
| `persistence.size`                   | PVC 스토리지 요청                        | `10Gi`                                                     |
| `nodeSelector`                       | 파드 할당을 위한 노드 라벨             | `{}`                                                       |

위의 매개 변수는 [bitnami/wordpress](http://github.com/bitnami/bitnami-docker-wordpress)에 정의 된 env 변수에 매핑됩니다. 자세한 내용은 [bitnami/wordpress](http://github.com/bitnami/bitnami-docker-wordpress) 이미지 설명서를 참조하십시오.

`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오. 예를 들면,

```console
$ helm install --name my-release \
  --set wordpressUsername=admin,wordpressPassword=password,mariadb.mariadbRootPassword=secretpassword \
    stable/wordpress
```

위 명령은 WordPress 관리자 계정 사용자 이름과 비밀번호를 각각`admin`과`password`로 설정합니다. 또한 MariaDB `root` 사용자 비밀번호를 `secretpassword`로 설정합니다.

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면,

```console
$ helm install --name my-release -f values.yaml stable/wordpress
```

> **Tip**: 기본 [values.yaml](values.yaml)을 사용할 수 있습니다.

## 영구적

[Bitnami WordPress](https://github.com/bitnami/bitnami-docker-wordpress) 이미지는 컨테이너의 `/bitnami` 경로에 WordPress 데이터 및 구성을 저장합니다.

영구 볼륨 클레임은 배포 전반에 걸쳐 데이터를 유지하는 데 사용됩니다. 이것은 GCE, AWS 및 minikube에서 작동하는 것으로 알려져 있습니다.
PVC를 구성하거나 지속성을 비활성화하려면 [Configuration](#configuration) 섹션을 참조하십시오.

## 외부 데이터베이스 사용

때로는 클러스터 내부에 데이터베이스를 설치하는 대신 Wordpress를 외부 데이터베이스에 연결하려고 할 수 있습니다. 관리되는 데이터베이스 서비스를 사용하거나 모든 응용 프로그램에 대해 단일 데이터베이스 서버를 실행하십시오. 이를 위해, 차트를 통해 [`externalDatabase` parameter](#configuration)에서 외부 데이터베이스의 자격 증명을 지정할 수 있습니다. `mariadb.enabled` 옵션을 사용하여 MariaDB 설치를 비활성화해야합니다. 예를 들어:

```console
$ helm install stable/wordpress \
    --set mariadb.enabled=false,externalDatabase.host=myexternalhost,externalDatabase.user=myuser,externalDatabase.password=mypassword,externalDatabase.database=mydatabase,externalDatabase.port=3306
```

또한 위의 MariaDB를 비활성화하면 `externalDatabase` 연결에 대한 값을 제공해야합니다.

## Ingress

이 차트는 수신 리소스를 지원합니다. 클러스터에 [nginx-ingress](https://kubeapps.com/charts/stable/nginx-ingress) 또는 [traefik](https://kubeapps.com/charts/stable/traefik) 수신 컨트롤러를 사용하여 WordPress 응용 프로그램을 제공 할 수 있습니다.

Ingress 통합을 활성화하려면`ingress.enabled`를 `true`로 설정하십시오.

### Hosts

이 WordPress 설치에 매핑되는 호스트 이름을 하나만 갖고 싶을 수도 있지만 둘 이상의 호스트를 가질 수도 있습니다. 이를 용이하게하기 위해 `ingress.hosts` 객체는 배열입니다.
각 항목에 대해 `name`, `tls`, `tlsSecret` 및 수신 컨트롤러가 알고 싶어 할 수있는 모든 `annotations`을 표시하십시오.
TLS를 표시하면 WordPress가 HTTPS URL을 생성하고 WordPress가 포트 443에 연결됩니다. `tlsSecret` 참조는이 차트에 의해 생성 될 필요는 없습니다.
그러나 TLS를 사용하면이 비밀이 존재할 때까지 수신 레코드가 작동하지 않습니다.

주석에 대해서는 [this document](https://github.com/kubernetes/ingress-nginx/blob/master/docs/annotations.md)를 참조하십시오.
모든 주석이 모든 주석 컨트롤러에서 지원되는 것은 아니지만이 문서는 널리 사용되는 많은 수신 컨트롤러에서 어떤 주석이 지원되는지를 잘 보여줍니다.

### TLS Secrets

이 차트는 수신 컨트롤러와 함께 사용할 TLS 비밀 생성을 용이하게하지만 필수는 아닙니다.
세 가지 일반적인 사용 사례가 있습니다:

* 키는 인증서 비밀을 생성 / 관리
* 사용자는 인증서를 별도로 생성 / 관리
* 추가 도구 (예 : [kube-lego] (https://kubeapps.com/charts/stable/kube-lego))는 응용 프로그램의 비밀을 관리합니다.

처음 두 경우에는 인증서와 키가 필요합니다. 우리는 그들이 이렇게 보일 것으로 기대합니다:

* 인증서 파일은 다음과 같아야합니다 (인증서 체인이있는 경우 둘 이상의 인증서가 있을 수 있음)

```
-----BEGIN CERTIFICATE-----
MIID6TCCAtGgAwIBAgIJAIaCwivkeB5EMA0GCSqGSIb3DQEBCwUAMFYxCzAJBgNV
...
jScrvkiBO65F46KioCL9h5tDvomdU1aqpI/CBzhvZn1c0ZTf87tGQR8NK7v7
-----END CERTIFICATE-----
```
* keys should look like:
```
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAvLYcyu8f3skuRyUgeeNpeDvYBCDcgq+LsWap6zbX5f8oLqp4
...
wrj2wDbCDCFmfqnSJ+dKI3vFLlEz44sAV8jX/kd4Y6ZTQhlLbYc=
-----END RSA PRIVATE KEY-----
````

Helm을 사용하여 인증서를 관리하려는 경우 해당 값을 지정된 `ingress.secrets` 항목의 `certificate` 및 `key` 값에 복사하십시오.

Helm 외부에서 TLS secret을 관리하려는 경우 다음을 수행하여 TLS secret을 작성할 수 있습니다.:

```
kubectl create secret tls wordpress.local-tls --key /path/to/key.key --cert /path/to/cert.crt
```

자세한 내용은 [this example](https://github.com/kubernetes/contrib/tree/master/ingress/controllers/nginx/examples/tls)를 참조하십시오.
