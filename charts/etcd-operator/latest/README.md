## 공식 문서

공식 프로젝트 문서 발견 [here](https://github.com/coreos/etcd-operator)

## 선행 조건

- Beta API가 활성화된 쿠버네티스 1.4 이상
- __Suggested:__ 백업 지원을 위한 기본 인프라의 PV 프로비저닝자 지원

## Chart 설치

릴리스 이름 `my-release`로 차트를 설치하려면 다음과 같이 하십시오:

```bash
$ helm install stable/etcd-operator --name my-release
```

__Note__: `cluster.enabled`를 설치하면 효과가 없다.
etcd 클러스터를 생성하기 전에 운영자가 TPR을 설치해야 하므로 헬름 설치 중에는 이 옵션이 무시되지만 업그레이드에는 사용할 수 있다.

## Chart 제거

`my-release` 배포를 제거/삭제하려면 다음과 같이 하십시오:

```bash
$ helm delete my-release
```

이 명령은 영구 볼륨을 제외한 모든 쿠버네티스 구성요소를 제거한다.

## 업데이트
TPR 리소스를 업데이트하면 TPR용 `kubectl apply`가 수정될 때까지 클러스터가 업데이트되지 않음([kubernetes/issues/29542](https://github.com/kubernetes/kubernetes/issues/29542) 참조)
옵션 주변의 작업들은 문서화된 [here](https://github.com/coreos/etcd-operator#resize-an-etcd-cluster)

## Configuration

다음 표에는 etcd-operator 차트의 구성 가능한 파라미터와 기본값이 나열되어 있다.

| 파라미터                                         | 내용                                                          | 기본값                                        |
| ------------------------------------------------- | -------------------------------------------------------------------- | ---------------------------------------------- |
| `rbac.create`                                     | 필요한 RBAC 서비스 계정, 역할, 역할 바인딩 설치        | `true`                                         |
| `rbac.apiVersion`                                 | RBAC API 버전 `v1alpha1|v1beta1`                                  | `v1beta1`                                      |
| `rbac.etcdOperatorServiceAccountName`             | RBAC가 활성일 때, 서비스 계정 리소스의 이름            | `etcd-operator-sa`                                      |
| `rbac.backupOperatorServiceAccountName`           | RBAC가 활성일 때, 서비스 계정 리소스의 이름            | `etcd-backup-operator-sa`                                      |
| `rbac.restoreOperatorServiceAccountName`          | RBAC가 활성일 때, 서비스 계정 리소스의 이름            | `etcd-restore-operator-sa`                                      |
| `deployments.etcdOperator`                        | etcd 클러스터 operator 배포                                     | `true`                                         |
| `deployments.backupOperator`                      | etcd 백업 operator 배포                                     | `true`                                         |
| `deployments.restoreOperator`                     | etcd 복원 operator 배포                                    | `true`                                         |
| `customResources.createEtcdClusterCRD`            | 커스텀 리소스 생성: EtcdCluster                                | `false`                                        |
| `customResources.createBackupCRD`                 | 커스텀 리소스 생성: EtcdBackup                              | `false`                                        |
| `customResources.createRestoreCRD`                | 커스텀 리소스 생성: EtcdRestore                             | `false`                                        |
| `etcdOperator.name`                               | Etcd Operator 이름                                                   | `etcd-operator`                                |
| `etcdOperator.replicaCount`                       | 생성할 operator 레플리카 수 (오직 1개만 지원됨)          | `1`                                            |
| `etcdOperator.image.repository`                   | etcd-operator 컨테이너 이미지                                        | `quay.io/coreos/etcd-operator`                 |
| `etcdOperator.image.tag`                          | etcd-operator 컨테이너 이미지 태그                                    | `v0.7.0`                                       |
| `etcdOperator.image.pullpolicy`                   | etcd-operator 컨테이너 이미지 pull 정책                            | `Always`                                       |
| `etcdOperator.resources.cpu`                      | etcd-operator 파드 당 CPU 제한                                      | `100m`                                         |
| `etcdOperator.resources.memory`                   | etcd-operator 파드 당 메모리 제한                                   | `128Mi`                                        |
| `etcdOperator.nodeSelector`                       | etcd operator 파드 할당을 위한 노드 라벨                         | `{}`                                           |
| `etcdOperator.commandArgs`                        | 추가적인 커맨드 인수                                         | `{}`                                           |
| `backupOperator.name`                             | 백업 operator 이름                                                 | `etcd-backup-operator`                         |
| `backupOperator.replicaCount`                     | 생성할 operator 레플리카 수 (오직 1개만 지원됨)          | `1`                                            |
| `backupOperator.image.repository`                 | operator 컨테이너 이미지                                             | `quay.io/coreos/etcd-operator`                 |
| `backupOperator.image.tag`                        | operator 컨테이너 이미지 태그                                         | `v0.7.0`                                       |
| `backupOperator.image.pullpolicy`                 | operator 컨테이너 이미지 pull 정책                                 | `Always`                                       |
| `backupOperator.resources.cpu`                    | etcd-operator 파드 당 CPU 제한                                      | `100m`                                         |
| `backupOperator.resources.memory`                 | etcd-operator 파드 당 메모리 제한                                   | `128Mi`                                        |
| `backupOperator.spec.storageType`                 | 백업 파일에 사용할 스토리지, 현재 오직 S3만 지원됨          | `S3`                                           |
| `backupOperator.spec.s3.s3Bucket`                 | 백업 파일을 저장할 S3 버킷                                    |                                                |
| `backupOperator.spec.s3.awsSecret`                | AWS 자격증명을 포함한 쿠버네티스 시크릿의 이름                |                                                |
| `backupOperator.nodeSelector`                     | etcd operator 파드 할당을 위한 노드 라벨                         | `{}`                                           |
| `backupOperator.commandArgs`                      | 추가적인 커맨드 인수                                         | `{}`                                           |
| `restoreOperator.name`                            | 복원 operator 이름                                                | `etcd-backup-operator`                         |
| `restoreOperator.replicaCount`                    | 생성할 operator 레플리카 수 (오직 1개만 지원됨)          | `1`                                            |
| `restoreOperator.image.repository`                | operator 컨테이너 이미지                                             | `quay.io/coreos/etcd-operator`                 |
| `restoreOperator.image.tag`                       | operator 컨테이너 이미지 태그                                         | `v0.7.0`                                       |
| `restoreOperator.image.pullpolicy`                | operator 컨테이너 이미지 pull 정책                                 | `Always`                                       |
| `restoreOperator.resources.cpu`                   | etcd-operator 파드 당 CPU 제한                                      | `100m`                                         |
| `restoreOperator.resources.memory`                | etcd-operator 파드 당 메모리 제한                                   | `128Mi`                                        |
| `restoreOperator.spec.s3.path`                    | 백업 파일을 포함한 S3 버킷의 경로                         |                                                |
| `restoreOperator.spec.s3.awsSecret`               | AWS 자격증명을 포함한 쿠버네티스 시크릿의 이름                |                                                |
| `restoreOperator.nodeSelector`                    | etcd operator pod 할당을 위한 노드 라벨                         | `{}`                                           |
| `restoreOperator.commandArgs`                     | 추가적인 커맨드 인수                                         | `{}`                                           |
| `etcdCluster.name`                                | etcd 클러스터 이름                                                    | `etcd-cluster`                                 |
| `etcdCluster.size`                                | etcd 클러스터 크기                                                    | `3`                                            |
| `etcdCluster.version`                             | etcd 클러스터 버전                                                 | `3.2.10`                                       |
| `etcdCluster.image.repository`                    | etcd 컨테이너 이미지                                                 | `quay.io/coreos/etcd-operator`                 |
| `etcdCluster.image.tag`                           | etcd 컨테이너 이미지 태그                                             | `v3.2.10`                                      |
| `etcdCluster.image.pullPolicy`                    | etcd 컨테이너 이미지 pull 정책                                     | `Always`                                       |
| `etcdCluster.enableTLS`                           | TLS 사용 활성화                                                    | `false`                                        |
| `etcdCluster.tls.static.member.peerSecret`        | TLS peer 인증서를 포함한 쿠버네티스 시크릿                          | `etcd-peer-tls`                                |
| `etcdCluster.tls.static.member.serverSecret`      | TLS server 인증서를 포함한 쿠버네티스 시크릿                        | `etcd-server-tls`                              |
| `etcdCluster.tls.static.operatorSecret`           | TLS client 인증서를 포함한 쿠버네티스 시크릿                        | `etcd-client-tls`                              |
| `etcdCluster.pod.antiAffinity`                    | etcd 클러스터 파드에 anti-affinity 필요 여부                | `false`                                        |
| `etcdCluster.pod.resources.limits.cpu`            | etcd 클러스터 파드 당 CPU 제한                                        | `100m`                                         |
| `etcdCluster.pod.resources.limits.memory`         | etcd 파드 당 메모리 제한                                    | `128Mi`                                        |
| `etcdCluster.pod.resources.requests.cpu`          | etcd 클러스터 파드 당 CPU 요청                                     | `100m`                                         |
| `etcdCluster.pod.resources.requests.memory`       | etcd 클러스터 파드 당 메모리 요청                                  | `128Mi`                                        |
| `etcdCluster.pod.nodeSelector`                    | etcd 클러스터 파드 할당을 위한 노드 라벨                          | `{}`                                           |

`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오. 예를 들면:

```bash
$ helm install --name my-release --set image.tag=v0.2.1 stable/etcd-operator
```

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면:

```bash
$ helm install --name my-release --values values.yaml stable/etcd-operator
```

## RBAC
기본적으로 차트는 권장 RBAC 역할과 역할 바인딩을 설치한다.

클러스터에서 이 실행을 지원하는지 확인하려면 다음과 같이 하십시오:

```console
$ kubectl api-versions | grep rbac
```

또한 api 서버에 다음 매개 변수를 가져야 한다. 사용 방법은 다음 문서를 참조하십시오 [RBAC](https://kubernetes.io/docs/admin/authorization/rbac/). 

```
--authorization-mode=RBAC
```

출력에 "베타" 또는 "알파"와 "베타"가 모두 포함된 경우, 기본적으로 rbac을 설치할 수 있으며, 그렇지 않은 경우 아래 설명된 대로 RBAC를 끌 수 있다.

### RBAC role/rolebinding 생성

RBAC 자원은 기본적으로 활성화된다. RBAC를 비활성화하려면 다음을 수행하십시오:

```console
$ helm install --name my-release stable/etcd-operator --set rbac.create=false
```

### RBAC 매니페스트 apiVersion 변경

기본적으로 RBAC 리소스는 "v1beta1" apiVersion으로 생성된다. "v1alpha1"을 사용하려면 다음을 수행하십시오.

```console
$ helm install --name my-release stable/etcd-operator --set rbac.install=true,rbac.apiVersion=v1alpha1
```
