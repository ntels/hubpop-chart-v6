## Configuration

다음 표에는 Fluentd 탄력 검사 차트의 구성 가능한 파라미터와 해당 기본값이 나열되어 있다.


| 파라미터                          | 내용                                | 기본값                                                    |
| ---------------------------------- | ------------------------------------------ | ---------------------------------------------------------- |
| `image.repository`                 | 이미지 레포지토리                                      | `guangbo/fluentd`                                          |
| `image.tag`                        | 이미지 태그                                  | `v1.0.0`                                                   |
| `image.pullPolicy`                 | 이미지 풀 정책                          | `IfNotPresent`                                             |
| `configMaps`                       | Fluentd 컨피그맵                         | `default conf files`                                       |
| `env`                              | Fluentd 파드에 추가할 환경변수 리스트   | ``                               |
| `nodeSelector`                     | 선택적 데몬셋 노드 셀렉터            | `{}`                                                       |
| `resources.limits.cpu`             | CPU 제한                                  | `500m`                                                     |
| `resources.limits.memory`          | Memory 제한                               | `200Mi`                                                    |
| `resources.requests.cpu`           | CPU 요청                                | `100m`                                                     |
| `resources.requests.memory`        | Memory 요청                             | `200Mi`                                                    |
| `service`                          | 서비스 정의                         | `{}`                                                       |
| `service.type`                     | 서비스 타입 (ClusterIP/NodePort)          | ClusterIP                                                  |
| `service.ports`                    | 서비스 포트 목록 [{name:...}...] | 미설정                                                    |
| `service.ports[].name`             | 서비스 포트 이름 중 하나                  | 미설정                                                    |
| `service.ports[].port`             | 서비스 포트                               | 미설정                                                    |
| `service.ports[].nodePort`         | 노드포트 포트(서비스 타입이 NodePort일 때) | 미설정                                                  |
| `service.ports[].protocol`         | 서비스 프로토콜(선택적, TCP/UDP일 수 있음) | 미설정                                                    |
| `tolerations`                      | 선택적 스테이트풀셋 허용           | `{}`                                                       |
| `annotations`                      | 선택적 스테이트풀셋 어노테이션           | `NULL`                                                     |
| `persistence.enabled`              | PVC를 사용한 영구 볼륨 활성               | `false`                                                    |
| `persistence.storageClass`         | PVC 스토리지 클래스                          | `nil` (알파 스토리지 클래스 어노테이션 사용)                |
| `persistence.accessMode`           | PVC 액세스 모드                            | `ReadWriteOnce`                                            |
| `persistence.size`                 | PVC 스토리지 요청                        | `10Gi`                                                     |
| `extraPersistence.enabled`         | PVC를 사용한 별도의 영구 볼륨 활성         | `false`                                                    |
| `extraPersistence.storageClass`    | PVC 별도의 스토리지 클래스                    | `nil` (알파 스토리지 클래스 어노테이션 사용)                |
| `extraPersistence.accessMode`      | PVC 별도의 액세스 모드                      | `ReadWriteOnce`                                            |
| `extraPersistence.size`            | PVC 별도의 스토리지 요청                  | `10Gi`                                                     |
| `extraPersistence.mountPath`       | PVC 별도의 마운트 경로                       | `/extra`                                                   |


`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오. 예를 들면:

```console
$ helm install --name my-release \
    stable/fluentd-aggregator
```

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면:

```console
$ helm install --name my-release -f values.yaml stable/fluentd-aggregator
```
