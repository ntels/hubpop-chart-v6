## ## 개요
Vault 운영자는 Kubernetes에서 [Vault][vault] 클러스터를 배포하고 관리합니다. Vault 운영자가 생성 한 Vault 인스턴스는 가용성이 높으며 자동 장애 조치 및 업그레이드를 지원합니다.

### 프로젝트 상태 : 베타
기본 기능이 완료되었으며 현재 주요 API 변경이 계획되지 않았지만 프로젝트가 안정적으로 선언되기 전에 API가 호환되지 않는 방식으로 변경 될 수 있습니다.

## 구성
파라미터 | 내용 | 기본값
--------- | ----------- | -------
`rbac.create` | true라면, RBAC 리소스 생성 및 사용 | `true`
`serviceAccounts.create` | true라면, values-operator 서비스 계정 생성 | `true`
`imagePullPolicy` | 모든 컨테이너 이미지 풀 정책 | `IfNotPresent`
`vaultOperator.replicaCount` | 원하는 vault operator 컨트롤러 파드 수 | `1`
`vaultOperator.image.repository` | vault operator 컨테이너 이미지 레포지토리 | `quay.io/coreos/vault-operator`
`vaultOperator.image.tag` | vault operator 컨테이너 이미지 태그 | `latest`
`vaultOperator.resources` | vault operator 파드 리소스 요청 및 제한 | `{}`
`vaultOperator.nodeSelector` | vault operator 파드 할당을 위한 노드 라벨 | `{}`
`vault.node` | 원하는 vault 클러스터 노드 수 | `2`
`vault.version` | vault APP 버전 | `0.9.1-0`
`etcd.image.repository` | etcd 컨테이너 이미지 레포지토리 | `quay.io/coreos/etcd-operator`
`etcd.image.tag` | etcd 컨테이너 이미지 태그 | `v0.8.3`
`ui.replicaCount` | 원하는 Vault UI 파드 | `1`
`ui.image.repository` | Vault UI 컨터이너 이미지 레포지토리 | `djenriquez/vault-ui`
`ui.image.tag` | Vault UI 컨테이너 이미지 태그 | `latest`
`ui.resources` | Vault UI 파드 리소스 요청 및 제한 | `{}`
`ui.nodeSelector` | Vault UI 파드 할당을 위한 노드 라벨 | `{}`
`ui.ingress.enabled` | true라면, Vault UI 인그레스가 생성됨 | `false`
`ui.ingress.annotations` | Vault UI 인그레스 어노테이션 | `{}`
`ui.ingress.hosts` | Vault UI 인그레스 호스트 네임 | `[]`
`ui.ingress.tls` | Vault UI 인그레스 TLS 구성 (YAML) | `[]`
`ui.vault.auth` | Vault UI 로그인 방법 | `TOKEN`
`ui.service.name` | Vault UI 서비스 네임 | `vault-ui`
`ui.service.type` | 생성할 ui 서비스의 타입 | `ClusterIP`
`ui.service.externalPort` | Vault UI 서비스 타겟 포트 | `8000`
`ui.service.internalPort` | Vault UI 컨테이너 포트 | `8000`
`ui.service.nodePort` | 서비스 노드 포트로 사용될 포트 (`server.service.type`이 `NodePort`가 아니라면 무시됨) | `0`


## Vault 클러스터 사용

배포 된 Vault 클러스터를 초기화, 개봉 및 사용하는 방법에 대해서는 [Vault usage guide](https://github.com/coreos/vault-operator/blob/master/doc/user/vault.md)를 참조하십시오.

Prometheus를 사용하여 Vault 클러스터를 모니터링하고 경고하는 방법은 [monitoring guide](https://github.com/coreos/vault-operator/blob/master/doc/user/monitoring.md)를 참조하십시오.

etcd opeartor를 사용하여 Vault 클러스터 데이터를 백업 및 복원하는 방법은 [recovery guide](https://github.com/coreos/vault-operator/blob/master/doc/user/recovery.md)를 참조하십시오.

기본 TLS 구성에 대한 개요 또는 Vault 클러스터에 대한 사용자 정의 TLS 자산을 지정하는 방법은 [TLS setup guide](https://github.com/coreos/vault-operator/blob/master/doc/user/tls_setup.md)를 참조하십시오.

[vault]: https://www.vaultproject.io/
[etcd-operator]: https://github.com/coreos/etcd-operator/
