## Chart 설치

추가 구성 방법에 대한 세부 정보를 포함한 전체 설치 지침
cert-manager의 기능성은 [getting started docs](https://cert-manager.readthedocs.io/en/latest/getting-started/)에서 찾을 수 있다.

릴리스 이름  `my-release`로 차트를 설치하려면 다음과 같이 하십시오.:

```console
$ helm install --name my-release stable/cert-manager
```

인증서 발행을 시작하려면 ClusterIssuer를 설정해야 함
또는 발급자 리소스(예: 'letsencrypt-staging' 발급자 생성).

다양한 유형의 발급자 및 구성 방법에 대한 자세한 정보
문서에서 찾을 수 있음:

https://cert-manager.readthedocs.io/en/latest/reference/issuers.html

자동으로 프로비저닝하도록 인증 관리자 구성 방법에 대한 정보
ingress 리소스 인증서, `ingress-shim` 보기
문서화:

https://cert-manager.readthedocs.io/en/latest/reference/ingress-shim.html

> **Tip**: `helm list`를 사용하여 모든 릴리즈 나열

## Chart 삭제

`my-release` 배포를 제거/삭제하려면 다음과 같이 하십시오.:

```console
$ helm delete my-release
```

이 명령은 차트와 관련된 모든 쿠버네티스 구성요소를 제거하고 릴리즈를 삭제한다.

## Configuration

다음 표에는 cert-manager 차트의 구성 가능한 매개변수와 기본값이 나열되어 있다.

| 파라미터 | 내용 | 기본값 |
| --------- | ----------- | ------- |
| `image.repository` | 이미지 레포지토리 | `quay.io/jetstack/cert-manager-controller` |
| `image.tag` | 이미지 태그 | `v0.5.2` |
| `image.pullPolicy` | 이미지 pull 정책 | `IfNotPresent` |
| `replicaCount`  | cert-manager 레플리카 수  | `1` |
| `createCustomResource` | 이 릴리즈로 CRD/TPR 생성 | `true` |
| `clusterResourceNamespace` | ClusterIssuer 리소스에 대한 DNS 공급자 자격 증명 등을 저장하는 데 사용된 네임스페이스를 재정의 | cert-manager 파드와 같은 네임스페이스
| `leaderElection.Namespace` | leader election을 위해 ConfigMap을 저장하는 데 사용된 네임스페이스를 재정의 | cert-manager 파드와 같은 네임스페이스
| `certificateResourceShortNames` | 인증서 CRD의 커스텀 별칭 | `["cert", "certs"]` |
| `extraArgs` | cert-manager의 선택적 플래그 | `[]` |
| `extraEnv` | cert-manager의 선택적 환경변수 | `[]` |
| `rbac.create` | `true`라면, RBAC 리소스 생성 및 사용 | `true` |
| `serviceAccount.create` | `true`라면, 새로운 서비스 계정 생성 | `true` |
| `serviceAccount.name` | 사용할 서비스 계정. 설정하지 않고 `serviceAccount.create`가 `true`라면, 풀네임 템플릿을 사용하여 네임이 생성됨 |  |
| `resources` | CPU/메모리 리소스 요청/제한 | |
| `nodeSelector` | 파드 할당을 위한 노드 라벨 | `{}` |
| `affinity` | 파드 할당을 위한 노드 선호도 | `{}` |
| `tolerations` | 파드 할당을 위한 노드 허용 | `[]` |
| `ingressShim.defaultIssuerName` | 인그레스 리소스에 사용할 선택적 기본 issuer |  |
| `ingressShim.defaultIssuerKind` | 인그레스 리소스에 사용할 선택적 기본 issuer 종류 |  |
| `ingressShim.defaultACMEChallengeType` | ACME issuer를 사용하는 인그레스에 사용할 선택적 기본 챌린지 타입  |  |
| `ingressShim.defaultACMEDNS01ChallengeProvider` | DNS01과 함께 ACME issuer를 사용하는 인그레스에 사용할 선택적 기본 DNS01 챌린지 provider |  |
| `podAnnotations` | cert-manager 파드에 추가할 어노테이션 | `{}` |
| `podDnsPolicy` | 선택적 cert-manager 파드 [DNS 정책](https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/#pods-dns-policy) |  |
| `podDnsConfig` | 선택적 cert-manager 파드 [DNS 구성](https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/#pods-dns-config) |  |
| `podLabels` | cert-manager 파드에 추가할 라벨 | `{}` |
| `http_proxy` | cert-manager 파드의 `HTTP_PROXY` 환경변수  값 | |
| `https_proxy` | cert-manager 파드의 `HTTPS_PROXY` 환경변수  값 | |
| `no_proxy` | cert-manager 파드의 `NO_PROXY` 환경변수  값 | |
| `webhook.enabled` | 유효성 검사 webhook 컴포넌트 설치 여부 토글 | `false` |
| `webhook.replicaCount` | cert-manager webhook 레플리카 수 | `1` |
| `webhook.podAnnotations` | webhook 파드에 추가할 어노테이션 | `{}` |
| `webhook.extraArgs` | cert-manager webhook 컴포넌트에 대한 선택적 플래그 | `[]` |
| `webhook.resources` | webhook 컴포넌트에 대한 CPU/메모리 리소스 요청/제한 | |
| `webhook.image.repository` | Webhook 이미지 레포지토리 | `quay.io/jetstack/cert-manager-webhook` |
| `webhook.image.tag` | Webhook 이미지 태그 | `v0.5.2` |
| `webhook.image.pullPolicy` | Webhook 이미지 pull 정책 | `IfNotPresent` |

`helm install`에 대한 `--set key=value[,key=value]` 인수를 사용하여 각 매개변수를 지정하십시오.

또는 차트를 설치하는 동안 위 파라미터의 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면,

```console
$ helm install --name my-release -f values.yaml .
```
> **Tip**: default [values.yaml](values.yaml)를 사용할 수 있다.

## 기여

이 차트는 [github.com/jetstack/cert-manager](https://github.com/jetstack/cert-manager/tree/master/contrib/charts/cert-manager)에서 유지된다.
