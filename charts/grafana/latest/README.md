## Configuration

파라미터 | 내용 | 기본값
--- | --- | ---
`routePrefix` | 레지스터 라우트에 사용되는 Prefix | `"/"`
`auth.anonymous.enabled` | true라면, 익명 인증 사용 | `true`
`adminUser` | Grafana 관리자 유저 네임 | `admin`
`adminPassword` | Grafana 관리자 유저 패스워드 | `admin`
`image.repository` |이미지 | `grafana/grafana`
`image.tag` | 이미지 태그 | `4.4.1`
`extraVars` | 그라파나 컨테이너에 추가 환경변수 전달 | `{}`
`grafanaWatcher.repository` | 이미지 | `quay.io/coreos/grafana-watcher`
`grafanaWatcher.tag` | 이미지 태그 | `v0.0.8`
`ingress.enabled` | true라면, Grafana 인그레스가 생성됨 | `false`
`ingress.annotations` | Grafana 인그레스에 대한 어노테이션 | `{}`
`ingress.labels` | Grafana 인그레스에 대한 라벨 | `{}`
`ingress.hosts` | Grafana 인그레스 적합한 도메인 네임 | `[]`
`ingress.tls` | Grafana 인그레스에 대한 TLS 구성 | `[]`
`nodeSelector` | 파드 할당을 위한 노드 라벨 | `{}`
`resources` | 파드 리소스 요청 & 제한 | `{}`
`service.annotations` | Grafana 서비스에 추가할 어노테이션 | `{}`
`service.clusterIP` | Grafana 서비스에 대한 클러스터-내부 IP 주소 | `""`
`service.externalIPs` | Grafana 서비스를 이용가능한 외부 IP 주소 목록 | `[]`
`service.loadBalancerIP` | Grafana 서비스에 할당할 외부 IP 주소 | `""`
`service.loadBalancerSourceRanges` | Grafana 서비스에 접근을 허용할 클라이언트 IP 리스트 | `[]`
`service.nodePort` | 각 노드에서 Grafana 서비스를 노출할 포트 | `30902`
`service.type` | Grafana 서비스 타입 | `ClusterIP`
`storageSpec` | 영구 데이터를 위한 Grafana 스토리지 스펙 | `{}`

`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오.
```

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다.

> **Tip**: 기본값 [values.yaml](values.yaml)을 사용할 수 있다.

> **Tip**: GCE에서 `Ingress.enabled=true`를 사용하려면 `service.type=NodePort`를 넣어야 한다.