## 전제 조건

- 베타 API가 활성화 된 Kubernetes 1.4 이상
- 기본 인프라에서 PV 프로비저너 지원

## 구성

다음 표는 MongoDB 차트의 구성 가능한 매개 변수 및 해당 기본값을 나열합니다.

| 파라미터                               | 내용                                                                                  | 기본값                                     |
| --------------------------------------- | -------------------------------------------------------------------------------------------- | ------------------------------------------- |
| `global.imageRegistry`                  | 글로벌 도커 이미지 레지스트리                                                                 | `nil`                                       |
| `image.registry`                        | MongoDB 이미지 레지스트리                                                                       | `docker.io`                                 |
| `image.repository`                      | MongoDB 이미지 이름                                                                           | `bitnami/mongodb`                           |
| `image.tag`                             | MongoDB 이미지 태그                                                                            | `{VERSION}`                                 |
| `image.pullPolicy`                      | 이미지 풀 정책                                                                            | `Always`                                    |
| `image.pullSecrets`                     | 이미지 풀 시크릿 지정                                                                   | `nil`                                       |
| `usePassword`                           | 패스워드 인증 활성                                                               | `true`                                      |
| `existingSecret`                        | MongoDB 인증서가 있는 기존 시크릿                                                     | `nil`                                       |
| `mongodbRootPassword`                   | MongoDB 관리자 패스워드                                                                       | `임의의 영숫자 문자열 (10)`           |
| `mongodbUsername`                       | MongoDB 커스텀 유저                                                                          | `nil`                                       |
| `mongodbPassword`                       | MongoDB 커스텀 유저 패스워드                                                                 | `임의의 영숫자 문자열 (10)`           |
| `mongodbDatabase`                       | 생성할 데이터베이스                                                                           | `nil`                                       |
| `mongodbEnableIPv6`                     | MongoDB에서 IPv6 활성/비활성 전환                                                     | `true`                                      |
| `mongodbExtraFlags`                     | MongoDB 추가적인 커맨드 인수                                                        | []                                          |
| `service.annotations`                   | 쿠버네티스 서비스 어노테이션                                                               | `{}`                                        |
| `service.type`                          | 쿠버네티스 서비스 타입                                                                      | `ClusterIP`                                 |
| `service.clusterIP`                     | 정적 클러스터 IP 또는 헤드리스 서비스에 대한 None                                               | `nil`                                       |
| `service.nodePort`                      | NodePort 서비스 타입에 바인딩할 포트                                                    | `nil`                                       |
| `port`                                  | MongoDB 서비스 포트                                                                         | `27017`                                     |
| `replicaSet.enabled`                    | 레플리카셋 구성 활성/비활성 전환                                           | `false`                                     |
| `replicaSet.name`                       | 레플리카셋 이름                                                                      | `rs0`                                       |
| `replicaSet.useHostnames`               | 레플리카셋 컨피그에서 DNS 호스트 네임 활성                                               | `true`                                      |
| `replicaSet.key`                        | 레플리카셋 인증에서 사용되는 키                                               | `nil`                                       |
| `replicaSet.replicas.secondary`         | 레플리카셋 secondary 노드 수                                                 | `1`                                         |
| `replicaSet.replicas.arbiter`           | 레플리카셋 arbiter 노드 수                                                   | `1`                                         |
| `replicaSet.pdb.minAvailable.primary`   | MongoDB Primary 노드에 대한 PDB                                                            | `1`                                         |
| `replicaSet.pdb.minAvailable.secondary` | MongoDB Secondary 노드에 대한 PDB                                                          | `1`                                         |
| `replicaSet.pdb.minAvailable.arbiter`   | MongoDB Arbiter 노드에 대한 PDB                                                            | `1`                                         |
| `podAnnotations`                        | 파드에 추가할 어노테이션                                                              | {}                                          |
| `podLabels`                             | 파드에 추가할 라벨                                                            | {}                                          |
| `resources`                             | 파드 리소스                                                                                | {}                                          |
| `nodeSelector`                          | 파드 할당을 위한 노드 라벨                                                               | {}                                          |
| `affinity`                              | 파드 할당을 위한 Affinity                                                                  | {}                                          |
| `tolerations`                           | 파드 할등을 위한 허용 라벨                                                         | {}                                          |
| `securityContext.enabled`               | 보안 컨텍스트 활성                                                                      | `true`                                      |
| `securityContext.fsGroup`               | 컨테이너에 대한 그룹 ID                                                                   | `1001`                                      |
| `securityContext.runAsUser`             | 컨테이너에 대한 유저 ID                                                                    | `1001`                                      |
| `persistence.enabled`                   | 영구 데이터를 위한 PVC 사용                                                                    | `true`                                      |
| `persistence.storageClass`              | 백업 PVC의 스토리지 클래스                                                                 | `nil` (알파 스토리지 클래스 어노테이션 사용) |
| `persistence.accessMode`                | 볼륨을 ReadOnly 또는 ReadWrite로 사용                                                          | `ReadWriteOnce`                             |
| `persistence.size`                      | 데이터 볼륨의 크기                                                                          | `8Gi`                                       |
| `persistence.annotations`               | 영구 볼륨 어노테이션                                                                | `{}`                                        |
| `persistence.existingClaim`             | 사용할 기존 PVC의 이름 (이것이 주어지면 하나를 만들지 않음)                        | `nil`                                       |
| `livenessProbe.initialDelaySeconds`     | liveness 프로브가 시작되기 전 지연                                                     | `30`                                        |
| `livenessProbe.periodSeconds`           | 프로브를 수행하는 빈도                                                               | `10`                                        |
| `livenessProbe.timeoutSeconds`          | 프로브 시간 초과                                                                     | `5`                                         |
| `livenessProbe.successThreshold`        | 프로브가 실패한 후 성공한 것으로 간주되는 최소 연속 성공 | `1`                                         |
| `livenessProbe.failureThreshold`        | 프로브가 성공한 후 실패한 것으로 간주되는 최소 연속 실패 | `6`                                         |
| `readinessProbe.initialDelaySeconds`    | readiness 프로브가 시작되기 전 지연                                                    | `5`                                         |
| `readinessProbe.periodSeconds`          | 프로브를 수행하는 빈도                                                               | `10`                                        |
| `readinessProbe.timeoutSeconds`         | 프로브 시간 초과                                                                     | `5`                                         |
| `readinessProbe.failureThreshold`       | 프로브가 실패한 후 성공한 것으로 간주되는 최소 연속 성공   | `6`                                         |
| `readinessProbe.successThreshold`       | 프로브가 성공한 후 실패한 것으로 간주되는 최소 연속 실패 | `1`                                         |
| `configmap`                             | 사용할 MongoDB 구성 파일                                                        | `nil`                                       |
| `metrics.enabled`                       | 사이드카 프로메테우스 exporter 활성                                                         | `false`                                     |
| `metrics.image.registry`                | MongoDB exporter 이미지 레지스트리                                                              | `docker.io`                                 |
| `metrics.image.repository`              | MongoDB exporter 이미지 이름                                                                  | `forekshub/percona-mongodb-exporter`                           |
| `metrics.image.tag`                     | MongoDB exporter 이미지 태그                                                                   | `latest`                                    |
| `metrics.image.pullPolicy`              | 이미지 풀 정책                                                                            | `IfNotPresent`                              |
| `metrics.image.pullSecrets`             | 도커 레지스트리 시크릿 이름을 배열로 지정                                             | `nil`                                       |
| `metrics.podAnnotations`                | 메트릭 exporter 파드에 대한 추가적인 어노테이션                                              | {}                                          |
| `metrics.resources`                     | Exporter 리소스 요청/제한                                                             | Memory: `256Mi`, CPU: `100m`             |
| `metrics.serviceMonitor.enabled`        | PrometheusOperator를 사용하여 메트릭 스크래핑을위한 ServiceMonitor 리소스 생성                 | `false`                                     |
| `metrics.serviceMonitor.additionalLabels`          | 설치된 Prometheus operator에 필요한 라벨을 전달하는 데 사용        | {}                                          |
| `metrics.serviceMonitor.relabellings`              | 스크랩 엔드포인트에 추가할 메트릭 라벨 재지정을 지정                         | `nil`                                       |
| `metrics.serviceMonitor.alerting.rules`            | 필요에 따라 개별 경고 규칙 정의                                      | {}                                          |
| `metrics.serviceMonitor.alerting.additionalLabels` | 설치된 Prometheus operator에 필요한 라벨을 전달하는 데 사용        | {}                                          |


`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오. 예를 들면,

```bash
$ helm install --name my-release \
  --set mongodbRootPassword=secretpassword,mongodbUsername=my-user,mongodbPassword=my-password,mongodbDatabase=my-database \
    stable/mongodb
```

위 명령은 MongoDB `root` 계정 암호를 `secretpassword`로 설정합니다. 또한 `my-database`이라는 데이터베이스에 액세스 할 수있는 비밀번호가`my-password` 인 `my-user`이라는 표준 데이터베이스 사용자를 작성합니다.

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면,

```bash
$ helm install --name my-release -f values.yaml stable/mongodb
```

> **Tip**: 기본값 [values.yaml](values.yaml)을 사용할 수 있습니다.

## 복제

다음 명령을 사용하여 레플리카 세트 모드에서 MongoDB 차트를 시작할 수 있습니다:

```bash
$ helm install --name my-release stable/mongodb --set replication.enabled=true
```

## 생산 설정 및 수평 스케일링

[values-production.yaml](values-production.yaml) 파일은 프로덕션 환경에 확장 가능하고 고 가용성 MongoDB 배치를 배치하기위한 구성으로 구성됩니다. 이 템플릿을 기반으로 프로덕션 구성을 설정하고 매개 변수를 적절하게 조정하는 것이 좋습니다.

```console
$ curl -O https://raw.githubusercontent.com/kubernetes/charts/master/stable/mongodb/values-production.yaml
$ helm install --name my-release -f ./values-production.yaml stable/mongodb
```

이 차트를 수평으로 확장하려면 다음 명령을 실행하여 MongoDB 레플리카 세트의 보조 노드 수를 조정하십시오.

```console
$ kubectl scale statefulset my-release-mongodb-secondary --replicas=3
```

이 차트의 일부 특징은 다음과 같습니다:

- 복제의 각 참가자는 stateful 세트를 가지고 있으므로 primary, secondary 또는 arbiter 노드를 찾을 위치를 항상 알 수 있습니다.
- secondary 및 arbiter 노드의 수는 독립적으로 확장 할 수 있습니다.
- Standalone MongoDB 서버를 사용하여 레플리카 세트를 사용하여 애플리케이션을 쉽게 이동할 수 있습니다.

## 새로운 인스턴스를 초기화

[Bitnami MongoDB](https://github.com/bitnami/bitnami-docker-mongodb) 이미지를 사용하면 사용자 지정 스크립트를 사용하여 새 인스턴스를 초기화 할 수 있습니다. 스크립트를 실행하려면 차트 폴더 `files/docker-entrypoint-initdb.d` 안에 있어야 스크립트가 ConfigMap으로 사용될 수 있습니다.

허용되는 확장자는`.sh`와`.js`입니다.

## 영구적

[Bitnami MongoDB](https://github.com/bitnami/bitnami-docker-mongodb) 이미지는 MongoDB 데이터 및 구성을 컨테이너의 `/bitnami/mongodb` 경로에 저장합니다.

이 차트는 이 위치에 [Persistent Volume](http://kubernetes.io/docs/user-guide/persistent-volumes/)을 마운트합니다. 볼륨은 동적 볼륨 프로비저닝을 사용하여 생성됩니다.

## 업그레이드

### 5.0.0까지

레플리카 세트 구성을 활성화 할 때 차트의 stateful 세트에 사용 된 라벨을 수정하지 않으면 이전 버전과의 호환성이 보장되지 않습니다.
아래 해결 방법을 사용하여 5.0.0 이전 버전에서 업그레이드하십시오. 다음 예제는 릴리스 이름이 `my-release`이라고 가정합니다:

```consoloe
$ kubectl delete statefulset my-release-mongodb-arbiter my-release-mongodb-primary my-release-mongodb-secondary --cascade=false
```
