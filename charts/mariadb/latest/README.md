## 소개

이 차트는 [Helm](https://helm.sh) 패키지 관리자를 사용하여 [Kubernetes](http://kubernetes.io) 클러스터에서 [MariaDB](https://github.com/bitnami/bitnami-docker-mariadb) 복제 클러스터 배포를 부트 스트랩합니다.

## Configuration

다음 표는 MariaDB 차트의 구성 가능한 매개 변수 및 해당 기본값을 나열합니다.

|             파라미터                     |                     내용                     |                              기본값                              |
|-------------------------------------------|-----------------------------------------------------|-------------------------------------------------------------------|
| `image.registry`                          | MariaDB 이미지 레지스트리                              | `docker.io`                                                       |
| `image.repository`                        | MariaDB 이미지 이름                                  | `bitnami/mariadb`                                                 |
| `image.tag`                               | MariaDB 이미지 태그                                   | `{VERSION}`                                                       |
| `image.pullPolicy`                        | MariaDB 이미지 풀 정책                           | 만약, `imageTag`가 `latest`라면 `Always`, 아니면 `IfNotPresent`           |
| `image.pullSecrets`                       | 이미지 풀 시크릿 지정                          | `nil` (배포된 파드에 이미지 풀 시크릿을 추가하지 않음)          |
| `service.type`                            | 쿠버네티스 서비스 타입                             | `ClusterIP`                                                       |
| `service.clusterIp`                       | 서비스 타입이 클러스터 IP일 때, 클러스터 IP 지정. 헤드리스 서비스에는 None 사용 | `nil`                                                                  |
| `service.port`                            | MySQL 서비스 포트                                  | `3306`                                                             |
| `rootUser.password`                       | `root` 유저에 대한 패스워드                        | _임의의 영숫자 10글자 문자열_                         |
| `rootUser.forcePassword`                  | 사용자에게 비밀번호 지정 강제                  | `false`                                                           |
| `db.user`                                 | 생성할 새로운 사용자의 유저 네임                      | `nil`                                                             |
| `db.password`                             | 새로운 사용자의 패스워드                           | _만약, `db.user`가 정의되었다면, 임의의 영숫자 10글자 문자열_ |
| `db.name`                                 | 생성할 새로운 데이터베이스의 이름                     | `my_database`                                                     |
| `replication.enabled`                     | MariaDB 레플리카 활성화                         | `true`                                                             |
| `replication.user`                        | MariaDB 레플리카 유저                            | `replicator`                                                       |
| `replication.password`                    | MariaDB 레플리카 유저 패스워드                   | _임의의 영숫자 10글자 문자열_                         |
| `master.annotations[].key`                | 어노테이션 리스트 아이템에 대한 키                |  `nil`                                                  |
| `master.annotations[].value`              | 어노테이션 리스트 아이템에 대한 값              |  `nil`                                                  |
| `master.affinity`                         | 마스터 affinity (설정된 경우, master.antiAffinity에 더함)  | `{}`                                                   |
| `master.antiAffinity`                     | 마스터 파드 anti-affinity 정책                     | `soft`                                                            |
| `master.tolerations`                      | 허용할 노드 목록 (마스터)            | `[]`                                                              |
| `master.persistence.enabled`              | `PersistentVolumeClaim`를 사용하여 영구 볼륨 활성  | `true`                                                            |
| `master.persistence.existingClaim`        | 기존 `PersistentVolumeClaim` 제공  | `nil`
| `master.persistence.mountPath`            | 기존 `PersistentVolumeClaim` 마운트 경로 구성  | `""`  
| `master.persistence.annotations`          | 영구 볼륨 클레임 어노테이션                 | `{}`                                                              |
| `master.persistence.storageClass`         | 영구 볼륨 스토리지 클래스                     | ``                                                                |
| `master.persistence.accessModes`          | 영구 볼륨 액세스 모드                      | `[ReadWriteOnce]`                                                 |
| `master.persistence.size`                 | 영구 볼륨 크기                              | `8Gi`                                                             |
| `master.config`                           | MariaDB 마스터 서버에 대한 컨피그 파일           | `_values.yaml 파일의 기본값_`                        |
| `master.resources`                        | 마스터 노드에 대한 CPU/Memory 리소스 요청/제한 | `{}`                                                              |
| `master.livenessProbe.enabled`            | liveness 프로브 활성 (마스터)             | `true`                                                            |
| `master.livenessProbe.initialDelaySeconds`| liveness 프로브가 시작되기 전 지연 (마스터)   | `120`                                                             |
| `master.livenessProbe.periodSeconds`      | 프로브를 수행하는 빈도 (마스터)             | `10`                                                              |
| `master.livenessProbe.timeoutSeconds`     | 프로브 시간 초과 (마스터)                   | `1`                                                               |
| `master.livenessProbe.successThreshold`   | 프로브의 최소 연속 성공 (마스터)| `1`                                                               |
| `master.livenessProbe.failureThreshold`   | 프로브의 최소 연속 실패 (마스터) | `3`                                                               |
| `master.readinessProbe.enabled`           | readiness 프로브 활성 (마스터)            | `true`                                                            |
| `master.readinessProbe.initialDelaySeconds`| readiness 프로브가 시작되기 전 지연 (마스터) | `30`                                                              |
| `master.readinessProbe.periodSeconds`     | 프로브를 수행하는 빈도 (마스터)             | `10`                                                              |
| `master.readinessProbe.timeoutSeconds`    | 프로브 시간 초과 (마스터)                   | `1`                                                               |
| `master.readinessProbe.successThreshold`  | 프로브의 최소 연속 성공 (마스터)| `1`                                                               |
| `master.readinessProbe.failureThreshold`  | 프로브의 최소 연속 실패  (마스터) | `3`                                                               |
| `slave.replicas`                          | 원하는 슬레이브 레플리카 수                    | `1`                                                               |
| `slave.annotations[].key`                 | 어노테이션 리스트 아이템에 대한 키                | `nil`                                                   |
| `slave.annotations[].value`               | 어노테이션 리스트 아이템에 대한 값              | `nil`                                                   |
| `slave.affinity`                          | 슬레이브 affinity (설정된 경우, slave.antiAffinity에 더함) | `{}`                                                      |
| `slave.antiAffinity`                      | 슬레이브 파드 anti-affinity 정책                      | `soft`                                                            |
| `slave.tolerations`                       | 허용할 노드 목록 (슬레이브)         | `[]`                                                              |
| `slave.persistence.enabled`               | `PersistentVolumeClaim`를 사용하여 영구 볼륨 활성  | `true`                                                            |
| `slave.persistence.annotations`           | 영구 볼륨 클레임 어노테이션                 | `{}`                                                              |
| `slave.persistence.storageClass`          | 영구 볼륨 스토리지 클래스                     | ``                                                                |
| `slave.persistence.accessModes`           | 영구 볼륨 액세스 모드                      | `[ReadWriteOnce]`                                                 |
| `slave.persistence.size`                  | 영구 볼륨 크기                              | `8Gi`                                                             |
| `slave.config`                            | MariaDB 슬레이브 레플리카에 대한 컨피그 파일          | `_values.yaml 파일의 기본값_`                        |
| `slave.resources`                         | 슬레이브 노드에 대한 CPU/Memory 리소스 요청/제한  | `{}`                                                              |
| `slave.livenessProbe.enabled`             | liveness 프로브 활성 (슬레이브)              | `true`                                                            |
| `slave.livenessProbe.initialDelaySeconds` | liveness 프로브가 시작되기 전 지연 (슬레이브)    | `120`                                                             |
| `slave.livenessProbe.periodSeconds`       | 프로브를 수행하는 빈도 (슬레이브)              | `10`                                                              |
| `slave.livenessProbe.timeoutSeconds`      | 프로브 시간 초과 (슬레이브)                    | `1`                                                               |
| `slave.livenessProbe.successThreshold`    | 프로브의 최소 연속 성공 (슬레이브) | `1`                                                               |
| `slave.livenessProbe.failureThreshold`    | 프로브의 최소 연속 실패  (슬레이브)  | `3`                                                               |
| `slave.readinessProbe.enabled`            | readness 프로브 활성 (슬레이브)             | `true`                                                            |
| `slave.readinessProbe.initialDelaySeconds`| readness 프로브가 시작되기 전 지연 (슬레이브)   | `45`                                                              |
| `slave.readinessProbe.periodSeconds`      | 프로브를 수행하는 빈도 (슬레이브)              | `10`                                                              |
| `slave.readinessProbe.timeoutSeconds`     | 프로브 시간 초과 (슬레이브)                    | `1`                                                               |
| `slave.readinessProbe.successThreshold`   | 프로브의 최소 연속 성공 (슬레이브) | `1`                                                               |
| `slave.readinessProbe.failureThreshold`   | 프로브의 최소 연속 실패 (슬레이브)  | `3`                                                               |
| `metrics.enabled`                         | 사이드카 프로메테우스 exporter 활성                | `false`                                                           |
| `metrics.image.registry`                           | Exporter 이미지 레지스트리                                 | `docker.io` |
| `metrics.image.repository`                           | Exporter 이미지 이름                                 | `prom/mysqld-exporter`                                            |
| `metrics.image.tag`                        | Exporter 이미지 태그                                  | `v0.10.0`                                                         |
| `metrics.image.pullPolicy`                 | Exporter 이미지 pull 정책                          | `IfNotPresent`                                                    |
| `metrics.resources`                       | Exporter 리소스 요청/제한                    | `nil`                                                             |

위의 매개 변수는 [bitnami/mariadb](http://github.com/bitnami/bitnami-docker-mariadb)에 정의 된 env 변수에 매핑됩니다. 자세한 내용은 [bitnami/mariadb](http://github.com/bitnami/bitnami-docker-mariadb) 이미지 설명서를 참조하십시오.

`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오. 예를 들면,

```bash
$ helm install --name my-release \
  --set root.password=secretpassword,user.database=app_database \
    stable/mariadb
```

위 명령은 MariaDB `root` 계정 암호를 `secretpassword`로 설정합니다. 또한 `my_database`이라는 데이터베이스를 만듭니다.

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면,

```bash
$ helm install --name my-release -f values.yaml stable/mariadb
```

> **Tip**: 기본 [values.yaml](values.yaml)을 사용할 수 있습니다.

## 새로운 인스턴스를 초기화

[Bitnami MariaDB](https://github.com/bitnami/bitnami-docker-mariadb) 이미지를 사용하면 사용자 지정 스크립트를 사용하여 새로운 인스턴스를 초기화 할 수 있습니다. 스크립트를 실행하려면 차트 폴더 `files/docker-entrypoint-initdb.d` 내에 있어야 스크립트가 ConfigMap으로 사용될 수 있습니다.

허용되는 확장명은 `.sh`, `.sql` 및 `.sql.gz`입니다.

## 영구적

[Bitnami MariaDB](https://github.com/bitnami/bitnami-docker-mariadb) 이미지는 MariaDB 데이터와 구성을 컨테이너의 `/bitnami/mariadb` 경로에 저장합니다.

이 차트는이 위치에 [Persistent Volume](kubernetes.io/docs/user-guide/persistent-volumes/) 볼륨을 마운트합니다. 볼륨은 기본적으로 동적 볼륨 프로비저닝을 사용하여 생성됩니다. 기존 PersistentVolumeClaim을 정의 할 수 있습니다.

## 업그레이드

준비/생존 프로브가 올바르게 작동하도록 업그레이드 할 때`rootUser.password` 매개 변수를 설정해야합니다. 이 차트를 처음 설치하면 '관리자 자격 증명'섹션에서 사용해야하는 자격 증명을 제공하는 일부 참고 사항이 표시됩니다. 비밀번호를 적어두고 아래 명령을 실행하여 차트를 업그레이드하십시오:

```bash
$ helm upgrade my-release stable/mariadb --set rootUser.password=[ROOT_PASSWORD]
```

| Note: placeholder _ [ROOT_PASSWORD] _를 설치 정보에서 얻은 값으로 대체해야합니다.

### 5.0.0까지

차트 배포에 사용 된 라벨을 수정하지 않으면 이전 버전과의 호환성이 보장되지 않습니다.
아래 해결 방법을 사용하여 5.0.0 이전 버전에서 업그레이드하십시오. 다음 예제에서는 릴리스 이름이 mariadb라고 가정합니다.:

```console
$ kubectl delete statefulset opencart-mariadb --cascade=false
```
