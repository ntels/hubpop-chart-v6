## Configuration

다음 표에는 도커 레지스트리 차트의 구성 가능한 파라미터와 해당 기본값이 나열되어 있다.

| 파라미터                   | 내용                                                                                | 기본값         |
|:----------------------------|:-------------------------------------------------------------------------------------------|:----------------|
| `image.pullPolicy`          | 컨테이너 pull 정책                                                                      | `IfNotPresent`  |
| `image.repository`          | 사용할 컨테이너 이미지                                                                     | `registry`      |
| `image.tag`                 | 배포할 컨테이너 이미지 태그                                                              | `2.6.2`         |
| `imagePullSecrets`          | 이미지 pull 시크릿 지정                                                                 | `nil` (배포된 파드에 이미지 pull 시크릿을 추가하지 않음) |
| `persistence.accessMode`    | PVC에 사용할 액세스 모드                                                                 | `ReadWriteOnce` |
| `persistence.enabled`       | 도커 스토리지에 PVC 사용 여부                                                | `false`         |
| `persistence.size`          | PVC에 요구할 공간의 양                                                           | `10Gi`          |
| `persistence.storageClass`  | PVC에 사용할 스토리지 클래스                                                               | `-`             |
| `persistence.existingClaim` | 구성에 사용할 기존 PVC 이름                                                  | `nil`           |
| `service.port`              | 서비스가 노출되는 TCP 포트                                                   | `5000`          |
| `service.type`              | 서비스 타입                                                                               | `ClusterIP`     |
| `service.clusterIP`         | `service.type`이 `ClusterIP`이고 이 값이 비어있지 않다면, 서비스의 클러스터IP를 설정 | `nil`           |
| `service.nodePort`          | `service.type`이 `NodePort`이고 이 값이 비어있지 않다면, 서비스의 노드포트를 설정 | `nil`           |
| `replicaCount`              | k8s 레플리카                                                                               | `1`             |
| `updateStrategy`            | 배포를 위한 업데이터 전략                                                             | `{}`            |
| `podAnnotations`            | 파드에 대한 어노테이션                                                                        | `{}`            |
| `resources.limits.cpu`      | 컨테이너 요청 CPU                                                                    | `nil`           |
| `resources.limits.memory`   | 컨테이너 요청 메모리                                                                 | `nil`           |
| `priorityClassName      `   | 우선 클래스 이름                                                                          | `""`            |
| `storage`                   | 사용할 스토리지 시스템                                                                      | `filesystem`    |
| `tlsSecretName`             | TLS 인증서에 대한 시크릿 이름                                                               | `nil`           |
| `secrets.htpasswd`          | Htpasswd 입증                                                                    | `nil`           |
| `secrets.s3.accessKey`      | S3 구성에 대한 액세스 키                                                            | `nil`           |
| `secrets.s3.secretKey`      | S3 구성에 대한 시크릿 키                                                            | `nil`           |
| `secrets.swift.username`    | Swift 구성에 대한 유저 네임                                                           | `nil`           |
| `secrets.swift.password`    | Swift 구성에 대한 비밀번호                                                           | `nil`           |
| `haSharedSecret`            | 레지스트리 공유 비밀                                                                 | `nil`           |
| `configData`                | 도커에 대한 구성 해쉬                                                              | `nil`           |
| `s3.region`                 | S3 리전                                                                                  | `nil`           |
| `s3.bucket`                 | S3 버킷 이름                                                                             | `nil`           |
| `s3.encrypt`                | 암호화된 형식으로 이미지 저장                                                           | `nil`           |
| `s3.secure`                 | HTTPS 사용                                                                                  | `nil`           |
| `swift.authurl`             | Swift 보안 URL                                                                              | `nil`           |
| `swift.container`           | Swift 컨테이너                                                                            | `nil`           |
| `nodeSelector`              | 파드 할당에 대한 노드 라벨                                                             | `{}`            |
| `tolerations`               | 파드 허용                                                                            | `[]`            |
| `ingress.enabled`           | true라면, 인그레스가 생성됨                                                           | `false`         |
| `ingress.annotations`       | 인그레스 어노테이션                                                                        | `{}`            |
| `ingress.path`              | 인그레스 서비스 경로                                                                       | `/`            |
| `ingress.hosts`             | 인그레스 호스트 네임                                                                          | `[]`            |
| `ingress.tls`               | 인그레스 TLS 구성 (YAML)                                                           | `[]`            |

`helm install`에서 `--set key=value[,key=value]` 인수를 사용하여 각 매개변수를 지정하십시오.

htpasswd 파일을 생성하려면 다음 docker 명령을 실행하십시오:
`docker run --entrypoint htpasswd registry:2 -Bbn user password > ./htpasswd`.
