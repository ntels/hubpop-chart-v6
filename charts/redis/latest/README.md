## 구성

다음 표는 Redis 차트의 구성 가능한 매개 변수 및 해당 기본값을 나열합니다.

| 파라미터                                  | 내용                                                                                                    | 기본값                              |
|--------------------------------------------|----------------------------------------------------------------------------------------------------------------|--------------------------------------|
| `image.registry`                           | Redis 이미지 레지스트리                                                                                           | `docker.io`                                          |
| `image.repository`                         | Redis 이미지 이름                                                                                               | `bitnami/redis`                                      |
| `image.tag`                                | Redis 이미지 태그                                                                                                | `{VERSION}`                                          |
| `image.pullPolicy`                         | 이미지 풀 정책                                                                                              | `Always`                                             |
| `image.pullSecrets`                        | docker-ragistry secret 이름을 배열로 지정                                                               | `nil`                                                |
| `cluster.enabled`                          | 마스터-슬레이브 토폴리지 사용                                                                                      | `true`                                               |
| `cluster.slaveCount`                       | 슬레이브 수                                                                                               | 1                                                    |
| `existingSecret`                           | 기존 시크릿 객체 이름 (비밀번호 인증)                                                   | `nil`                                                |
| `usePassword`                              | 패스워드 사용                                                                                                   | `true`                                               |
| `password`                                 | Redis 패스워드 (기존 시크릿이 설정된 경우 무시)                                                                 | 무작위 생성                                   |
| `networkPolicy.enabled`                    | 네트워크 정책 활성                                                                                           | `false`                                              |
| `networkPolicy.allowExternal`              | 연결에 클라이언트 라벨이 필요하지 않음                                                                     | `true`                                               |
| `metrics.enabled`                          | 사이드카 프로메테우스 exporter 활성                                                                           | `false`                                              |
| `metrics.image.registry`                   | Redis 이미지 레지스트리                                                                                           | `docker.io`                                          |
| `metrics.image.repository`                 | Redis 이미지 이름                                                                                               | `bitnami/redis`                                      |
| `metrics.image.tag`                        | Redis 이미지 태그                                                                                                | `{VERSION}`                                          |
| `metrics.image.pullPolicy`                 | 이미지 풀 정책                                                                                              | `IfNotPresent`                                       |
| `metrics.image.pullSecrets`                | docker-ragistry secret 이름을 배열로 지정                                                               | `nil`                                                |
| `metrics.podLabels`                        | 메트릭 exporter 파드에 대한 추가적인 라벨                                                                     | {}                                                   |
| `metrics.podAnnotations`                   | 메트릭 exporter 파드에 대한 추가적인 어노테이션                                                                | {}                                                   |
| `metrics.targetServiceAnnotations`         | 모니터링할 서비스에 대한 어노테이션 (redis 마스터 및 redis 슬레이브 서비스)                                | {}                                                   |
| `metrics.resources`                        | Exporter 리소스 요청/제한                                                                               | Memory: `256Mi`, CPU: `100m`                         |
| `master.persistence.enabled`               | PVC를 사용하여 데이터 유지 (마스터 노드)                                                                        | `true`                                               |
| `master.persistence.path`                  | 다른 이미지를 사용하기 위해 볼륨을 마운트하는 경로                                                               | `/bitnami`                                           |
| `master.persistence.subPath`               | 마운트할 볼륨의 서브 디렉토리                                                                         | `""`                                                 |
| `master.persistence.storageClass`          | 백업 PVC의 스토리지 클래스                                                                                   | `generic`                                            |
| `master.persistence.accessModes`           | 영구 볼륨 액세스 모드                                                                                 | `[ReadWriteOnce]`                                    |
| `master.persistence.size`                  | 데이터 볼륨의 사이즈                                                                                            | `8Gi`                                                |
| `master.podLabels`                         | Redis 마스터 파드에 대한 추가적인 라벨                                                                         | {}                                                   |
| `master.podAnnotations`                    | Redis 마스터 파드에 대한 추가적인 어노테이션                                                                    | {}                                                   |
| `master.port`                              | Redis 마스터 포트                                                                                              | 6379                                                 |
| `master.args`                              | Redis 마스터 커맨드 인수                                                                                 | []                                                   |
| `master.disableCommands`                   | 비활성화 할 쉼표로 구분된 Redis 커맨드 목록 (마스터)                                                     | `FLUSHDB,FLUSHALL`                                   |
| `master.extraFlags`                        | Redis 마스터 추가 커맨드 라인 플래그                                                                     | []                                                   |
| `master.nodeSelector`                      | 포드 할당을 위한 Redis 마스터 노드 라벨                                                                   | {"beta.kubernetes.io/arch": "amd64"}                 |
| `master.tolerations`                       | Redis 마스터 파드 할당을 위한 허용 라벨                                                              | []                                                   |
| `master.service.type`                      | 쿠버네티스 서비스 타입 (레디스 마스터)                                                                         | `LoadBalancer`                                       |
| `master.service.annotations`               | Redis 마스터 서비스에 대한 어노테이션                                                                           | {}                                                   |
| `master.service.loadBalancerIP`            | Redis 마스터 서비스 타입이 `LoadBalancer`일 때, loadBalancerIP                                                  | `nil`                                                |
| `master.securityContext.enabled`           | 보안 컨텍스트 활성화 (레디스 마스터 파드)                                                                     | `true`                                               |
| `master.securityContext.fsGroup`           | 컨테이너에 대한 그룹 ID (레디스 마스터 파드)                                                                  | `1001`                                               |
| `master.securityContext.runAsUser`         | 컨테이너에 대한 유저 ID (레디스 마스터 파드)                                                                   | `1001`                                               |
| `master.resources`                         | 레디스 마스터 CPU/Memory 리소스 요청/제한                                                               | Memory: `256Mi`, CPU: `100m`                         |
| `master.livenessProbe.enabled`             | liveness 프로브 활성 (레디스 마스터 파드)                                                              | `true`                                               |
| `master.livenessProbe.initialDelaySeconds` | liveness 프로브가 시작되기 전 지연 (레디스 마스터 파드)                                                    | `30`                                                 |
| `master.livenessProbe.periodSeconds`       | 프로브를 수행하는 빈도 (레디스 마스터 파드)                                                              | `30`                                                 |
| `master.livenessProbe.timeoutSeconds`      | 프로브 시간 초과 (레디스 마스터 파드)                                                                    | `5`                                                  |
| `master.livenessProbe.successThreshold`    | 프로브가 실패한 후 성공한 것으로 간주되는 최소 연속 성공 (레디스 마스터 파드) | `1`                                                  |
| `master.livenessProbe.failureThreshold`    | 프로브가 성공한 후 실패한 것으로 간주되는 최소 연속 실패 (레디스 마스터 파드)                    | `5`                                                  |
| `master.readinessProbe.enabled`            | readiness 프로브 활성 (레디스 마스터 파드)                                                             | `true`                                               |
| `master.readinessProbe.initialDelaySeconds`| readiness 프로브가 시작되기 전 지연 (레디스 마스터 파드)                                                   | `5`                                                  |
| `master.readinessProbe.periodSeconds`      | 프로브를 수행하는 빈도 (레디스 마스터 파드)                                                              | `10`                                                 |
| `master.readinessProbe.timeoutSeconds`     | 프로브 시간 초과 (레디스 마스터 파드)                                                                    | `1`                                                  |
| `master.readinessProbe.successThreshold`   | 프로브가 실패한 후 성공한 것으로 간주되는 최소 연속 성공 (레디스 마스터 파드)| `1`                                                  |
| `master.readinessProbe.failureThreshold`   | 프로브가 성공한 후 실패한 것으로 간주되는 최소 연속 실패 (레디스 마스터 파드)                     | `5`                                                  |
| `slave.serviceType`                        | 쿠버네티스 서비스 타입 (레디스 슬레이브)                                                                          | `LoadBalancer`                                       |
| `slave.service.annotations`                | 레디스 슬레이브 서비스에 대한 어노테이션                                                                            | {}                                                   |
| `slave.service.loadBalancerIP`             | Redis 슬레이브 서비스 타입이 `LoadBalancer`일 때, loadBalancerIP                                                   | `nil`                                                |
| `slave.port`                               | Redis 슬레이브 포트                                                                                               | `master.port`                                        |
| `slave.args`                               | Redis 슬레이브 커맨드 인수                                                                                  | `master.args`                                        |
| `slave.disableCommands`                    | 비활성화 할 쉼표로 구분된 Redis 커맨드 목록 (슬레이브)                                                      | `master.disableCommands`                             |
| `slave.extraFlags`                         | Redis 슬레이브 추가 커맨드 라인 플래그                                                                      | `master.extraFlags`                                  |
| `slave.livenessProbe.enabled`              | slave.liveness 프로브 활성 (레디스 슬레이브 파드)                                                               | `master.livenessProbe.enabled`                       |
| `slave.livenessProbe.initialDelaySeconds`  | slave.liveness 프로브가 시작되기 전 지연 (레디스 슬레이브 파드)                                                     | `master.livenessProbe.initialDelaySeconds`           |
| `slave.livenessProbe.periodSeconds`        | 프로브를 수행하는 빈도 (레디스 슬레이브 파드)                                                               | `master.livenessProbe.periodSeconds`                 |
| `slave.livenessProbe.timeoutSeconds`       | 프로브 시간 초과 (레디스 슬레이브 파드)                                                                     | `master.livenessProbe.timeoutSeconds`                |
| `slave.livenessProbe.successThreshold`     | 프로브가 실패한 후 성공한 것으로 간주되는 최소 연속 성공 (레디스 슬레이브 파드)  | `master.livenessProbe.successThreshold`              |
| `slave.livenessProbe.failureThreshold`     | 프로브가 성공한 후 실패한 것으로 간주되는 최소 연속 실패 (레디스 슬레이브 파드)                    | `master.livenessProbe.failureThreshold`              |
| `slave.readinessProbe.enabled`             | slave.readiness 프로브 활성 (레디스 슬레이브 파드)                                                        | `master.readinessProbe.enabled`                      |
| `slave.readinessProbe.initialDelaySeconds` | slave.readiness 프로브가 시작되기 전 지연 (레디스 슬레이브 파드)                                              | `master.readinessProbe.initialDelaySeconds`          |
| `slave.readinessProbe.periodSeconds`       | 프로브를 수행하는 빈도 (레디스 슬레이브 파드)                                                               | `master.readinessProbe.periodSeconds`                |
| `slave.readinessProbe.timeoutSeconds`      | 프로브 시간 초과 (레디스 슬레이브 파드)                                                                     | `master.readinessProbe.timeoutSeconds`               |
| `slave.readinessProbe.successThreshold`    | 프로브가 실패한 후 성공한 것으로 간주되는 최소 연속 성공 (레디스 슬레이브 파드)  | `master.readinessProbe.successThreshold`             |
| `slave.readinessProbe.failureThreshold`    | 프로브가 성공한 후 실패한 것으로 간주되는 최소 연속 실패 (레디스 슬레이브 파드)   | `master.readinessProbe.failureThreshold`             |
| `slave.podLabels`                          | Redis 슬레이브 파드에 대한 추가적인 라벨                                                                          | `master.podLabels`                                   |
| `slave.podAnnotations`                     | Redis 슬레이브 파드에 대한 추가적인 어노테이션                                                                     | `master.podAnnotations`                              |
| `slave.securityContext.enabled`            | 보안 컨텍스트 활성화 (레디스 슬레이브 파드)                                                                      | `master.securityContext.enabled`                     |
| `slave.securityContext.fsGroup`            | 컨테이너에 대한 그룹 ID (레디스 슬레이브 파드)                                                                   | `master.securityContext.fsGroup`                     |
| `slave.securityContext.runAsUser`          | 컨테이너에 대한 유저 ID (레디스 슬레이브 파드)                                                                    | `master.securityContext.runAsUser`                   |
| `slave.resources`                          | Redis 슬레이브 CPU/Memory 리소스 요청/제한                                                                | `master.resources`                                   |

위의 파라미터는 [bitnami/redis](http://github.com/bitnami/bitnami-docker-redis)에 정의 된 env 변수에 매핑됩니다. 자세한 내용은 [bitnami/redis](http://github.com/bitnami/bitnami-docker-redis) 이미지 설명서를 참조하십시오.

`--set key=value[,key=value]` 인수에서 `helm install`로 각 파라미터를 지정하십시오. 예를 들면,

```bash
$ helm install --name my-release \
  --set password=secretpassword \
    stable/redis
```

위 명령은 Redis 서버 비밀번호를`secretpassword`로 설정합니다.

또는 차트를 설치하는 동안 매개변수 값을 지정하는 YAML 파일을 제공할 수 있다. 예를 들면,

```bash
$ helm install --name my-release -f values.yaml stable/redis
```

> **Tip**: 기본 [values.yaml](values.yaml)을 사용할 수 있습니다.
> **Note for minikube users**: minikube의 현재 버전 (작성 당시 v0.24.1)은 루트로만 쓸 수있는 `hostPath` 영구 볼륨을 제공합니다. 차트 기본값을 사용하면 Redis 포드가 `/bitnami` 디렉토리에 쓰려고 시도 할 때 포드 실패가 발생합니다. `--set persistence.enabled=false`로 Redis를 설치하는 것을 고려하십시오. 자세한 내용은 minikube issue [1990](https://github.com/kubernetes/minikube/issues/1990)를 참조하십시오.

## 네트워크 정책

Redis에 대한 네트워크 정책을 활성화하려면 [a networking plugin that implements the Kubernetes NetworkPolicy spec](https://kubernetes.io/docs/tasks/administer-cluster/declare-network-policy#before-you-begin)을 설치하고 `networkPolicy.enabled`를 `true`로 설정하십시오.

Kubernetes v1.5 및 v1.6의 경우 DefaultDeny 네임 스페이스 주석을 설정하여 NetworkPolicy도 설정해야합니다.
참고 : 네임 스페이스에서 _all_ 포드에 대한 정책이 적용됩니다.

    kubectl annotate namespace default "net.beta.kubernetes.io/network-policy={\"ingress\":{\"isolation\":\"DefaultDeny\"}}"

NetworkPolicy를 활성화하면 생성 된 클라이언트 라벨이있는 포드 만 Redis에 연결할 수 있습니다. 이 라벨은 성공적인 설치 후 출력에 표시됩니다.

## 영구적

[Bitnami Redis](https://github.com/bitnami/bitnami-docker-redis) 이미지는 Redis 데이터 및 구성을 컨테이너의 `/bitnami` 경로에 저장합니다.

기본적으로 차트는 이 위치에 [Persistent Volume](http://kubernetes.io/docs/user-guide/persistent-volumes/)을 마운트합니다. 볼륨은 동적 볼륨 프로비저닝을 사용하여 생성됩니다. 영구 볼륨 클레임이 이미 있으면 설치 중에 지정하십시오.

기본적으로 차트는 데이터와 구성을 모두 유지합니다. 데이터 디렉토리 만 유지하려면 `persistence.path`를 `/bitnami/redis/data`로, `persistence.subPath`를 `redis /data`로 설정하십시오.

### 기존 영구 볼륨 볼륨

1. PersistentVolume 생성
1. PersistentVolumeClaim 생성
1. 차트를 설치

```bash
$ helm install --set persistence.existingClaim=PVC_NAME redis
```

## 측정 항목

차트는 선택적으로 [prometheus](https://prometheus.io)에 대한 메트릭 내보내기를 시작할 수 있습니다. 메트릭 엔드포인트(포트 9121)가 서비스에 노출됩니다. [example Prometheus scrape configuration](https://github.com/prometheus/prometheus/blob/master/documentation/examples/prometheus-kubernetes.yml)에 설명 된 것과 유사한 것을 사용하여 클러스터 내에서 메트릭을 스크랩 할 수 있습니다. 클러스터 외부에서 메트릭을 스크랩해야하는 경우 Kubernetes API 프록시를 사용하여 엔드 포인트에 액세스 할 수 있습니다.