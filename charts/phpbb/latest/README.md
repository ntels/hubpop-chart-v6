# phpBB

[phpBB](https://www.phpbb.com/)는 PHP 스크립팅 언어로 작성된 인터넷 포럼 패키지입니다. "phpBB"라는 이름은 PHP 게시판의 약자입니다.

## 소개

이 차트는 [Helm](https://helm.sh) 패키지 관리자를 사용하여 [Kubernetes](http://kubernetes.io) 클러스터에서 [phpBB](https://github.com/bitnami/bitnami-docker-phpbb) 배포를 부트 스트랩합니다.

또한 phpBB 애플리케이션의 데이터베이스 요구 사항을 위해 MariaDB 배치를 부트 스트랩하는 데 필요한 [Bitnami MariaDB chart](https://github.com/kubernetes/charts/tree/master/stable/mariadb)을 패키지화합니다.

## 구성

다음 표는 phpBB 차트의 구성 가능한 매개 변수 및 해당 기본값을 나열합니다.

|             파라미터             |              내용              |                         기본값                         |
|-----------------------------------|---------------------------------------|---------------------------------------------------------|
| `image.registry`                  | phpBB 이미지 레지스트리                  | `docker.io`                                             |
| `image.repository`                | phpBB 이미지 이름                      | `bitnami/phpbb`                                         |
| `image.tag`                       | phpBB 이미지 태그                       | `{VERSION}`                                             |
| `image.pullPolicy`                | 이미지 풀 정책                     | `imageTag`가 `latest`라면 `Always`, 아니면 `IfNotPresent` |
| `image.pullSecrets`               | 이미지 풀 시크릿 지정            | `nil`                                                   |
| `phpbbUser`                       | 어플리케이션의 유저               | `user`                                                  |
| `phpbbPassword`                   | 어플리케이션 패스워드                  | _임의의 영숫자 10글자 문자열_          |
| `phpbbEmail`                      | 관리자 이메일                           | `user@example.com`                                      |
| `allowEmptyPassword`              | DB 빈 패스워드 허용              | `yes`                                                   |
| `smtpHost`                        | SMTP 호스트                             | `nil`                                                   |
| `smtpPort`                        | SMTP 포트                             | `nil`                                                   |
| `smtpUser`                        | SMTP 유저                             | `nil`                                                   |
| `smtpPassword`                    | SMTP 패스워드                         | `nil`                                                   |
| `externalDatabase.host`           | 외부 데이터베이스의 호스트         | `nil`                                                   |
| `externalDatabase.user`           | 외부 DB의 기존 유저  | `bn_phpbb`                                              |
| `externalDatabase.password`       | 위 기존 유저의 패스워드       | `nil`                                                   |
| `externalDatabase.database`       | 기존 DB의 이름         | `bitnami_phpbb`                                         |
| `mariadb.enabled`                 | MariaDB 차트 사용 여부          | `true`                                                  |
| `mariadb.rootUser.password`     | MariaDB admin password                | `nil`                                                   |
| `mariadb.db.name`         | 생성할 데이터베이스 이름               | `bitnami_phpbb`                                         |
| `mariadb.db.user`             | 생성할 데이터베이스 유저               | `bn_phpbb`                                              |
| `mariadb.db.password`         | 생성할 데이터베이스 패스워드             | _임의의 영숫자 10글자 문자열_          |
| `serviceType`                     | 쿠버네티스 서비스 타입               | `LoadBalancer`                                          |
| `persistence.enabled`             | PVC를 사용하여 영구 볼륨 활성          | `true`                                                  |
| `persistence.apache.storageClass` | 아파치 볼륨에 대한 PVC 스토리지 클래스   | `nil` (알파 스토리지 클래스 어노테이션 사용)             |
| `persistence.apache.accessMode`   | 아파치 볼륨에 대한 PVC 액세스 모드     | `ReadWriteOnce`                                         |
| `persistence.apache.size`         | 아파치 볼륨에 대한 PVC 스토리지 요청 | `1Gi`                                                   |
| `persistence.phpbb.storageClass`  | phpBB 볼륨에 대한 PVC 스토리지 클래스    | `nil` (알파 스토리지 클래스 어노테이션 사용)             |
| `persistence.phpbb.accessMode`    | phpBB 볼륨에 대한 PVC 액세스 모드      | `ReadWriteOnce`                                         |
| `persistence.phpbb.size`          | phpBB 볼륨에 대한 PVC 스토리지 요청  | `8Gi`                                                   |
| `resources`                       | CPU/Memory 리소스 요청/제한   | Memory: `512Mi`, CPU: `300m`                            |

위의 매개 변수는 [bitnami/phpbb](http://github.com/bitnami/bitnami-docker-phpbb)에 정의 된 env 변수에 매핑됩니다. 자세한 내용은 [bitnami/phpbb](http://github.com/bitnami/bitnami-docker-phpbb) 이미지 설명서를 참조하십시오.

## 영구적

[Bitnami phpBB](https://github.com/bitnami/bitnami-docker-phpbb) 이미지는 컨테이너의 `/bitnami/phpbb` 및 `/bitnami/apache` 경로에 phpBB 데이터 및 구성을 저장합니다.

영구 볼륨 클레임은 배포 전반에 걸쳐 데이터를 유지하는 데 사용됩니다. 이것은 GCE, AWS 및 minikube에서 작동하는 것으로 알려져 있습니다.
PVC를 구성하거나 지속성을 비활성화하려면 [Configuration](#configuration) 섹션을 참조하십시오.